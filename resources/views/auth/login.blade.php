@extends('layouts.app')



@section('content')

    <div class="form-title">

        <h4>Login</h4>

    </div>

    <!-- Login Form-->

    <div class="login-form text-center">

        <div class="toggle"><!--<i class="fa fa-user-plus"></i>-->

        </div>

        <div class="form formLogin">

            <h2>Login to your account</h2>

            <form method="POST" action="{{ route('login') }}">

               @csrf

                @if(Session::has('message'))

                        <div class='alert alert-success'>

                        {{ Session::get('message') }}

                        @php

                        Session::forget('message');

                        @endphp

                        </div>

                @endif

            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

            @error('email')

                                    <span class="invalid-feedback" role="alert">

                                        <strong>{{ $message }}</strong>

                                    </span>

            @enderror

            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')

                                    <span class="invalid-feedback" role="alert">

                                        <strong>{{ $message }}</strong>

                                    </span>

                                @enderror 

                <div class="remember text-left">

                    <div class="checkbox checkbox-primary">

                        <input id="checkbox2" type="checkbox" checked>

                        <label for="checkbox2">

                            Remember me

                        </label>

                    </div>

                </div>

                <button>Login</button>

            @if (Route::has('password.request'))

                                    <a class="btn btn-link" href="{{ route('password.request') }}">

                                        {{ __('Forgot Your Password?') }}

                                    </a>

                                @endif

            </form>

        </div>

                <div class="form formReset">

            <h2>Reset your password?</h2>

            <form method="POST" action="{{ route('password.email') }}">

                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        <button type="submit" class="btn btn-primary">

                                    {{ __('Send Password Reset Link') }}

                         </button>

            </form>

        </div>

      </div>





@endsection

