@include('header')

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>

  <style>

   .error{ color:red; } 

  </style>

<div class="page-content-wrapper">

                <div class="page-content" style="min-height:1271px">

                    <div class="page-bar">

                        <div class="page-title-breadcrumb">

                            <div class=" pull-left">

                                <div class="page-title">Add  File Upload </div>

                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">

                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                            <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="{{ url('import_list') }}"> List</a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li class="active">Add  File</li>

                            </ol>

                        </div>

                    </div>

<div class="main-panel">

    <div class="content-wrapper">

       

            <div class="row">



                <div class="col-12 grid-margin stretch-card">

                    <div class="card">

                        <div class="card-body">

                            <h4 class="card-title">Upload File</h4>

                          <form  id="customerform" method="POST" action="{{ url('import_list/csvfileupload') }}" enctype="multipart/form-data">

                                            @csrf

                                            @if(Session::has('message'))

                                                    <div class='alert alert-success'>

                                                    {{ Session::get('message') }}

                                                    @php

                                                    Session::forget('message');

                                                    @endphp

                                                    </div>

                                            @endif

                                <div class="form-group row">

                

                                    <div class="col">

                                        <label> Please Select File (XLXS only)::<span class="required-mark">*</span></label>

                                        <input type="file" name="csvfile" class="form-control"  placeholder="Enter company name" >

                                         <span class="text-danger">{{ $errors->first('xlxsfile') }}</span> 

                                    </div>  

 

                              </div>

                              <div class="form-group row">

                

                                

                      

                              </div>

                              <div class="form-group row">

                               <button type="submit" class="btn btn-primary mr-2">Submit</button>

                             </div>

                              </div>

   

                            </form>

                        </div>

                    </div>

                </div>



            </div>

    </div>

</div>

</div>

</div>

@include('footer')