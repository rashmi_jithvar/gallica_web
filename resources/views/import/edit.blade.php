@include('header')
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
  <style>
   .error{ color:red; } 
  </style>
<div class="page-content-wrapper">
                <div class="page-content" style="">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Update Client </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="{{ url('client') }}"> Client</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Update Client</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Update Client</h4>
                          <form  id="customerform" method="POST" action="{{ action('ClientController@update',$id) }}" enctype="multipart/form-data">
                                            @csrf
                                            @if(Session::has('message'))
                                                    <div class='alert alert-success'>
                                                    {{ Session::get('message') }}
                                                    @php
                                                    Session::forget('message');
                                                    @endphp
                                                    </div>
                                            @endif
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Company Name:<span class="required-mark">*</span></label>
                                        <input type="text"  id="company_name" class="form-control"  value="{{$user->company_name}}" name="company_name" >
                                         <span class="text-danger">{{ $errors->first('company_name') }}</span> 
                                    </div>
  
                                    <div class="col">
                                        <label>First Name:<span class="required-mark">*</span></label>
                                        <input type="text" id="first_name" class="form-control"  value="{{$user->first_name}}" name="first_name" >
                                         <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                        
                                    </div>
                                    <div class="col">
                                        <label>Last Name:</label>
                                        <input type="text" id="last_name" class="form-control"  value="{{$user->last_name}}" name="last_name" >
                                         <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                        
                                    </div>
                              </div>
                              <div class="form-group row">    
                                    <div class="col">
                                        <label>Mobile:<span class="required-mark">*</span></label>
                                        <input type="number"  id="mobile" class="form-control"  value="{{$user->mobile}}" name="mobile" maxlength="10" minlength="10" >
                                         <span class="text-danger">{{ $errors->first('mobile') }}</span>
                                        
                                    </div>
                                 
                                    <div class="col">
                                        <label>Email :<span class="required-mark">*</span></label>
                                        <input type="text" id="email" class="form-control"  value="{{$user->email}}" name="email" >
                                         <span class="text-danger">{{ $errors->first('email') }}</span>
                                        
                                    </div>
                           
                                    <div class="col">
                                        <label>Website :</label>
                                        <input type="text" id="website" class="form-control"  value="{{$user->website}}" name="website" >
                                         <span class="text-danger">{{ $errors->first('website') }}</span>
                                        
                                    </div>
                              </div> 
                              <div class="form-group row">
                                    <div class="col">
                                      <label>Address Line 1 :<span class="required-mark">*</span></label> 
                                        <input type="text" id="address_line_1" class="form-control"  value="{{$user->address_line_1}}" name="address_line_1" >
                                         <span class="text-danger">{{ $errors->first('address_line_1') }}</span>
                                        
                                    </div>

                             
                                    <div class="col">
                                      <label>Address Line 2 :</label> 
                                        <input type="text" id="address_line_2" class="form-control"  value="{{$user->address_line_2}}" name="address_line_2" >
                                         <span class="text-danger">{{ $errors->first('address_line_2') }}</span>
                                        
                                    </div>
                    
                                    <div class="col">
                                            <label for="exampleSelectGender">State<span class="required-mark">*</span></label>
                                                  <input type="text"  id="state" class="form-control"  value="{{$user->state}}" name="state" >
                                              <span class="text-danger">{{ $errors->first('state') }}</span>  
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <label for="exampleSelectGender">City<span class="required-mark">*</span>
                                        </label>
                                          <input type="text"  id="city" class="form-control"  value="{{$user->city}}" name="city" >
                                              <span class="text-danger">{{ $errors->first('city') }}</span> 
                                  </div>
                                <div class="col">
                                    <label>Pincode<span class="required-mark">*</span>
                                    </label>
                                    <input type="number" class="form-control" id="pincode" value="{{$user->pincode}}" name="pincode" maxlength="6" minlength="6">
                                    <span class="text-danger">{{ $errors->first('pincode') }}
                                    </span>      
                                  </div>
                                <div class="col">
                                        <label>Logo:</label>
                                        <input type="hidden" class="form-control" id="old_image" value="{{$user->image}}" name="old_image" >
                                        <input type="file" class="form-control" id="image"  name="image" >
                                      <span class="text-danger">{{ $errors->first('image') }}</span>    
                                </div>
                              </div>
                          <div class="form-group row">
                
                                    <div class="col">
                                        <label>GST Number:</label>
                                        <input type="text"  id="gst_number" class="form-control"  value="{{$user->gst_number}}" name="gst_number" >
                                         <span class="text-danger">{{ $errors->first('gst_number') }}</span> 
                                    </div>  
                                    <div class="col">
                                        <label>Credit Period:</label>
                                        <input type="text" id="credit_period" class="form-control"  placeholder="Enter credit period in days example 15" name="credit_period" value="{{$user->credit_period}}">
                                         <span class="text-danger">{{ $errors->first('credit_period') }}</span>
                                        
                                    </div>
                                    <div class="col">
                                        <label>Credit Period Defination:<span class="required-mark">*</span></label>
                                       <select class="form-control" name="credit_period_defination">
                                         <option disabled selected>--select---</option>
                                         <option value='1' @if($user->credit_period_defination=='1') selected='selected' @endif>Calculation from billing date</option>
                                         <option value='2' @if($user->credit_period_defination=='2') selected='selected' @endif>Calculation from Acceptance</option>
                                         <option value='3' @if($user->credit_period_defination=='3') selected='selected' @endif>Calculation from Finance</option>
                                         
                                       </select>
                                         <span class="text-danger">{{ $errors->first('credit_period_defination') }}</span>
                                        
                                    </div>
                              </div> 
                              <div class="form-group row">
                
                                    <div class="col">
                                        <label>SPOC from Business Development:</label>
                                      <select name="spoc_business" id="spoc_business" class="form-control select2">
                                         <option disabled selected>Select</option>
                                        @if (isset($role_business))
                                      @foreach($role_business as $key => $dropdownGroup)
                                      <option value ="{{$key}}" @if($user->spoc_business== $key) selected='selected' @endif>{{ $dropdownGroup }} </option>
                                       @endforeach
                                       @endif
                                    </select>
                                       
                                         <span class="text-danger">{{ $errors->first('spoc_business') }}</span> 
                                    </div>  
                                    <div class="col">
                                        <label>SPOC from Ops:</label>
                                    <select name="spoc_ops" id="spoc_ops" class="form-control select2">
                                         <option disabled selected>Select</option>
                                        @if (isset($role_ops))
                                      @foreach($role_ops as $key => $dropdownGroup)
                                      <option value ="{{$key}}" @if($user->spoc_ops== $key) selected='selected' @endif>{{ $dropdownGroup }} </option>
                                       @endforeach
                                       @endif
                                    </select>
                                         <span class="text-danger">{{ $errors->first('spoc_ops') }}</span>
                                        
                                    </div>
                                    <div class="col">
                                        <label>SPCO from Finance:</label>
                                    <select name="spoc_finance" id="spoc_finance" class="form-control select2">
                                         <option disabled selected>Select</option>
                                        @if (isset($role_finance))
                                      @foreach($role_finance as $key => $dropdownGroup)
                                      <option value ="{{$key}}" @if($user->spoc_finance== $key) selected='selected' @endif>{{ $dropdownGroup }} </option>
                                       @endforeach
                                       @endif
                                    </select>
                                         <span class="text-danger">{{ $errors->first('spoc_finance') }}</span>
                                        
                                    </div>
                              </div>  

                              <div class="form-group row">
                
                                    <div class="col-md-4">
                                        <label>First Contact SPOC:</label>
                                      <select name="first_contact_spoc" id="first_contact_spoc" class="form-control">
                                         <option disabled selected>Select SPOC</option>
                                        @if (isset($userdropdown))
                                      @foreach($userdropdown as $key => $dropdownGroup)
                                      <option value ="{{$key}}" @if($user->first_contact_spoc== $key) selected='selected' @endif>{{ $dropdownGroup }} </option>
                                       @endforeach
                                       @endif
                                    </select>
                                         <span class="text-danger">{{ $errors->first('first_contact_spoc') }}</span> 
                                    </div>  
                                  <div class="col-md-4">
                                        <label>Warehouse:</label>
                                      <select name="warehouse_id" id="warehouse_id" class="warehouse form-control select2-multiple" multiple>
                                       
                                      @if (isset($client_warehouse))
                                      @if (isset($warehousedropdown))
                                      @foreach($client_warehouse as $key_c => $client)
                                      @foreach($warehousedropdown as $key => $dropdownGroup)
                                      <option value ="{{$key}}" @if($client->warehouse_id== $key) selected='selected' @endif>{{ $dropdownGroup }} </option>
                                       @endforeach
                                        @endforeach
                                       @endif
                                       @endif
                                    </select>
                                         <span class="text-danger">{{ $errors->first('warehouse_id') }}</span> 
                                    </div>   
                                    <div class="col">
                                   
                                        
                                    </div>
                              </div>                                                           
                                <div class="form-group">
                                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                </div>  
                              </div>

                               
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>
<script>
 if ($("#customerform").length > 0) {
    $("#customerform").validate({
     
    rules: {
    first_name: {
        required: true,
        maxlength: 150
      },
    company_name: {
        required: true,
        maxlength: 255
      },
    mobile: {
        required: true,
        maxlength: 10,
        number: true,

      },
    email: {
        required: true,
        email: true,
      },
    address_line_1: {
        required: true,
      },
   
    state: {
          required: true,
          maxlength: 150,
    }, 
    city: {
        required: true,
      },
   
    pincode: {
          required: true,
          maxlength: 6,
    }, 
    credit_period_defination: {
          required: true,
          maxlength: 50,
    }, 
    password: {
          required: true,
          maxlength: 50,

    }, 
    confirm_password: {
        required: true,
       equalTo: "#password"
      
      },
    website: {  
        url: true
    },
    first_contact_spoc: {
          required: true,
          maxlength: 255,
    }, 
    warehouse_id: {
          required: true,
          
    }, 
   
  },
    messages: {
       
      first_name: {
        required: "Please Enter First name",
        maxlength: "Your last name maxlength should be 50 characters long."
      },

    company_name: {
        required: "Please Enter Company name",
        maxlength: "Your company_name should be 255 characters long."
      },
    mobile: {
        required: "Please Enter Mobile number",
        maxlength: "Your mobile should be 10 characters long."
      },  
    email: {
        required: "Please Enter Email address",
          email: "Please enter valid email",
        
      },
    address_line_1: {
        required: "Please Enter Address",
       
      }, 
    state: {
          required: "Please Enter state",
          
        },
    city: {
          required: "Please Enter City",
          
      },
    pincode: {
          required: "Please Enter pincode",
          
      },
    credit_period_defination: {
          required: "Please Enter credit_period_defination",
          
        },
    first_contact_spoc: {
          required: "Please Select First Contact SPOC",
          
        },
    password: {
          required: "Please Enter Password",
          
        },
    website: {
          required: "Please Enter valid url",
          
        },
    warehouse_id: {
          required: "Please Select Warehouse",
          
        },    
   
       
    },
    })
  }
</script>
@include('footer')