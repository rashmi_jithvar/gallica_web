@include('header')
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"></div>
                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="{{ url('import_list') }}"> List</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">View </li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
                      <div class="col-sm-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header>View</header>
                                 </div>
                                 <div class="card-body ">
                                 <div class="table-scrollable">
                                  <table id="mainTable" class="table table-striped">
                                  <thead>

                                  </thead>
                                  <tbody>
                                      <tr>
                                          <th>Title</th>
                                          <td>{{ $list ? $list->title: ''}}</td>
                                      </tr>
                                      <tr>
                                          <th>Author</th>
                                          <td>{{$list ? $list->author:''}}</td>
                                      </tr>
                                      <tr>
                                          <th>Publisher</th>
                                          <td>{{$list ? $list->publisher:''}}</td>
                                      </tr>
                                      <tr>
                                          <th>Publication Date</th>
                                          <td>{{$list ? $list->publication_date:''}}</td>
                                      </tr>                                      
                                      <tr>
                                          <th>Type  </th>
                                          <td>{{$list ? $list->type:'' }}</td>
                                      </tr>
                                      <tr>
                                          <th>Type</th>
                                          <td>{{$list ? $list->sub_type:''}}</td>
                                      </tr>                                      
                                      <tr>
                                          <th>Language</th>
                                          <td>{{$list ? $list->language:''}}</td>
                                      </tr>
                                      <tr>
                                          <th>Language</th>
                                          <td>{{$list ? $list->sub_language:''}}</td>
                                      </tr>                                     
                                       <tr>
                                          <th>Format</th>
                                          <td>{{$list ? $list->format : ''}}</td>
                                      </tr>
                                      <tr>
                                          <th>Format</th>
                                          <td>{{$list ? $list->format_1:''}}</td>
                                      </tr>                                     
                                       <tr>
                                          <th>Description</th>
                                          <td>{{$list ? $list->description:''}}</td>
                                      </tr>
                                      <tr>
                                          <th>Rights</th>
                                          <td>{{$list ? $list->rights:''}}</td>
                                      </tr>  
                                      <tr>
                                          <th>Identifier</th>
                                          <td>{{$list ? $list->identifier:''}}</td>
                                      </tr>                                        
                                      <tr>
                                          <th>Source</th>
                                          <td>{{$list ? $list->source:''}}</td>
                                      </tr>                                        
                                      <tr>
                                          <th>Relation</th>
                                          <td>{{$list ? $list->relation:''}}</td>
                                      </tr>                                       
                                       <tr>
                                          <th>Provenance</th>
                                          <td>{{$list ? $list->provenance:''}}</td>
                                      </tr>                                        
                                      <tr>
                                          <th>Date Of Online Available</th>
                                          <td>{{$list  ? $list->date_of_online_available:''}}</td>
                                      </tr> 

                                                                                                                 
                                  </tbody>
                                  <tfoot>
                                  </tfoot>
                              </table>
                              </div>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>
            </div>
@include('footer')
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                