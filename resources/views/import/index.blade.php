@include('header')
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">List </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                       
                            <li><i class="fa fa-plus"></i>&nbsp;<a class="parent-item" href="{{ url('import_list/create') }}">Upload  File</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                
                                <li class="active">List</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
           
            	<a href="{{ url('import_list/create') }}" class="btn btn-info" role="button">Import CSv File</a>
            
            	

<br><br>
              <h4 class="card-title">List </h4>
              <div class="row">
                 @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                  @endif
                <div class="col-12">
                  <div class="table-responsive">

                    <button style="margin-bottom: 10px" class="btn btn-danger delete_all" data-url="{{ url('/import_list/bulkDelete') }}">Delete All Selected</button>
                    
                    <table id="example1" class="display" style="width:100%;">
                      <thead>
                        <tr>
                           <th><input type="checkbox" id="master"> &nbsp;Select </th>
                            <th>Sr.no.</th>
                            <th>Url</th>
                            <th>Status</th> 
<!--                        <th>Author</th>
                            <th>Publisher</th>
                            <th>Publication_date</th>
                            <th>Type</th>
                            <th>Language</th>
                            <th>Date Of Online Available</th> -->
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php $i = 0
                        @endphp
                        @foreach($list as $ware)
                        @php $i++
                        @endphp
                        <tr>
                            <td><input type="checkbox" class="sub_chk" data-id="{{ $ware->id ?? '' }}"></td>
                            <td>{{$i}}</td>
                            <td>{{$ware ? $ware->url:''}}</td>
<!--                              <td>{{$ware ? $ware->author:''}}</td>
                              <td>{{$ware ? $ware->publisher:''}}</td>
                               <td>{{$ware ? $ware->publication_date:''}}</td>
                                <td>{{$ware ? $ware->type:''}}</td>
                                 <td>{{$ware ? $ware->language:''}}</td>
                                  <td>{{$ware ? $ware->date_of_online_available:''}}</td> -->
                            
                           <!-- <td>
                              <img src="public/images/{{$ware->image}}" width="80"></td>-->
                            <td>
                              <?php  if($ware->flag_id =='0'){
                                  echo'<button type="button" class="btn btn-warning">Pending</button>
';
                              }
                              else{
                                  echo '<button type="button" class="btn btn-success">Updated</button>
';
                              }?>
                              
                            </td>  
                            <td>
            
                        
                        <a href="{{action('ImportController@show',$ware->id)}}" title="view"><i class="fa fa-eye" aria-hidden="true"></i></a>
                         
                         <a href="{{action('ImportController@destroy', $ware->id)}}"  ><i class="fa fa-trash" aria-hidden="true" title="Delete" Onclick="return ConfirmDelete();"></i></a>
                      
              
                              
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@include('footer')
<script type="text/javascript">
    $(document).ready(function () {


        $('#master').on('click', function(e) {
         if($(this).is(':checked',true))  
         {
            $(".sub_chk").prop('checked', true);  
         } else {  
            $(".sub_chk").prop('checked',false);  
         }  
        });


        $('.delete_all').on('click', function(e) {


            var allVals = [];  
            $(".sub_chk:checked").each(function() {  
                allVals.push($(this).attr('data-id'));
            });  


            if(allVals.length <=0)  
            {  
                alert("Please select row.");  
            }  else {  


                var check = confirm("Are you sure you want to delete this row?");  
                if(check == true){  


                    var join_selected_values = allVals.join(","); 


                    $.ajax({
                        url: $(this).data('url'),
                        type: 'DELETE',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+join_selected_values,
                        success: function (data) {
                            if (data['success']) {
                                $(".sub_chk:checked").each(function() {  
                                    $(this).parents("tr").remove();
                                });
                                alert(data['success']);
                            } else if (data['error']) {
                                alert(data['error']);
                            } else {
                                alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });


                  $.each(allVals, function( index, value ) {
                      $('table tr').filter("[data-row-id='" + value + "']").remove();
                  });
                }  
            }  
        });


        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            onConfirm: function (event, element) {
                element.trigger('confirm');
            }
        });


        $(document).on('confirm', function (e) {
            var ele = e.target;
            e.preventDefault();


            $.ajax({
                url: ele.href,
                type: 'DELETE',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (data) {
                    if (data['success']) {
                        $("#" + data['tr']).slideUp("slow");
                        alert(data['success']);
                    } else if (data['error']) {
                        alert(data['error']);
                    } else {
                        alert('Whoops Something went wrong!!');
                    }
                },
                error: function (data) {
                    alert(data.responseText);
                }
            });


            return false;
        });
    });
</script>