@include('header')
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Role Permission </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="{{ url('role_permission/create') }}">Add Role Permission</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>                                
                                </li>
                                <li class="active">Role Permission</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
            	<!--<a href="{{ url('role_permission/create') }}" class="btn btn-info" role="button">Create Role Permission</a>-->

<br><br>
              <h4 class="card-title">Role Permission </h4>
              <div class="row">

                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>Sr.no.</th>
                            <th>Permission </th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                        @foreach($roles as $role)
                        <tr>
                            <td>{{$role->id}}</td>
                            <td>{{$role->action_name}}</td>
                            <td>{{$role->name}}</td>
                            <td>
                         @if($role->status == '1')
                               <label class="badge badge-success">Active</label>
                          
                          @else
                                <label class="badge badge-danger">Pending</label>
                           @endif
                              
                        
                            </td>
                            <td>
            
                    <form action="{{action('RolePermissionController@destroy', $role->id)}}" method="post">
                    {{csrf_field()}}
                        <input name="_method" type="hidden" value="DELETE">
                           <!--   <a href="{{action('RolePermissionController@edit',$role->id)}}" class="btn btn-info" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>-->
                        <button class="btn btn-danger" type="submit" Onclick="return ConfirmDelete();"><i class="fa fa-trash" ></i></button>
                    </form>
                              
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@include('footer')