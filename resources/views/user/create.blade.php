@include('header')
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">User </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="{{ url('user') }}"> User</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>                                       
                                <li class="active">Add User</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add User</h4>
                          <form  id="customerform" method="POST" action="{{ url('user/store') }}">
                                            @csrf
                                            @if(Session::has('message'))
                                                    <div class='alert alert-success'>
                                                    {{ Session::get('message') }}
                                                    @php
                                                    Session::forget('message');
                                                    @endphp
                                                    </div>
                                            @endif
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>FullName:<span class="required-mark">*</span></label>
                                        <input type="text"  id="name" class="form-control"  placeholder="Enter Fullname" name="name" >
                                         <span class="text-danger">{{ $errors->first('name') }}</span> 
                                    </div>
                                    <div class="col">
                                        <label>Email:<span class="required-mark">*</span></label>
                                        <input type="email" id="email" class="form-control"  placeholder="Enter Email" name="email" >
                                         <span class="text-danger">{{ $errors->first('email') }}</span>
                                        
                                    </div>
                                    <div class="col">
                                        <label >Role Id:<span class="required-mark">*</span></label>
                                      <select name="role_id" id="role_id" class="form-control" required="">
                                        <option disabled selected>Select RoleId</option>
                                        @if (isset($roledropdown))
                                      @foreach($roledropdown as $key => $dropdownGroup)
                                      <option value ="{{$key}}">{{ $dropdownGroup }} </option>
                                       @endforeach
                                       @endif
                                    </select>
                                       <span class="text-danger">{{ $errors->first('role_id') }}</span>  
                                    </div>
                                </div>
                               <!-- <div class="form-group row" id="client" style="display: none;">
                                <div class="col"></div>
                                <div class="col"></div>
                                <div class="col">
                                    <label >ClientId:<span class="required-mark">*</span></label>
                                      <select name="client_id" id="client_id" class="form-control" required="">
                                        <option value='0'>Select Client</option>
                                        @if (isset($clientdropdown))
                                      @foreach($clientdropdown as $key => $cliedownGroup)
                                      <option value ="{{$key}}">{{ $cliedownGroup }} </option>
                                       @endforeach
                                       @endif
                                    </select>
                               
                                    </div>
                                </div>-->
                                <div class="form-group row">
                                 <!-- <div class="col">
                                        <label>Username:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Username" name="username" >
                                       <span class="text-danger">{{ $errors->first('username') }}</span>   
                                        
                                    </div>-->
                                <div class="col">
                                        <label>Password:<span class="required-mark">*</span></label>
                                        <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password" >
                                      <span class="text-danger">{{ $errors->first('password') }}</span>    
                                </div>
                                <div class="col">
                                        <label>Confirm Password:<span class="required-mark">*</span></label>
                                        <input type="password" class="form-control" id="confirm_password" placeholder="Enter Confirm Password" name="confirm_password" >
                                    <span class="text-danger">{{ $errors->first('confirm_password') }}</span>      
                                </div>
                                <div class="col">
                                        <label for="exampleSelectGender">Status:<span class="required-mark">*</span></label>
                                        <select class="form-control select2" id="status_a" name="status" required>
                                            <option disabled selected>Select Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                        <span class="text-danger">{{ $errors->first('status_a') }}</span> 
                              </div>
                                    
                              </div>
                              <div class="form-group row">
     
                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
        $('#role_id').on('change',function(){
          var selValue = $('#role_id').val();  
          if(selValue =='5'){
            $("#client").show();
          }
          else{
            $("#client").hide();
          }
          
})
</script>
<script>
   if ($("#customerform").length > 0) {
    $("#customerform").validate({
     
    rules: {
    name: {
        required: true,
        maxlength: 150
      },
    email: {
        required: true,
        email: true,
      },
    role_id: {
        required: true,
      },
   
    password: {
          required: true,
          maxlength: 50,

    }, 
    confirm_password: {
        required: true,
       equalTo: "#password"
      
      },
   
    status_a: {
          required: true,
         
    },    
  },
    messages: {
       
      name: {
        required: "Please Enter First name",
        maxlength: "Your last name maxlength should be 150 characters long."
      },


    email: {
        required: "Please Enter Email address",
          email: "Please enter valid email",
        
      },
    role_id: {
        required: "Please Select Role id",
       
      }, 
    password: {
          required: "Please Enter Password",
          
        },
    status_a: {
          required: "Please select status",
          
      },
       
    },
    })
  }
</script>
@include('footer')