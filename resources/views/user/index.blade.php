@include('header')
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">User </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                              @if(in_array("UserController@create", $routeArray))
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('/home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                @endif
                                <li class="active">User</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
        @if(in_array("UserController@create", $routeArray))
            	<a href="{{ url('user/create') }}" class="btn btn-info" role="button">Create User</a>
              @endif
              

<br><br>
              <h4 class="card-title">User </h4>
              <div class="row">

                <div class="col-12">
                  <div class="table-responsive">
                    <table id="example1"  class="table" style="width:100%;">
                      <thead>
                        <tr>
                            <th>Sr.no.</th>
                            <th>Name </th>
                          
                            <th>Email</th>
                            <th>Status</th>
                            <th>Role Id</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php $i = 0
                         @endphp
                          @foreach($user as $user)
                          @php $i++
                        @endphp
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$user->name}}</td>
                           
                            <td>{{$user->email}}</td>
                             <td>{{$user->role_name}}</td>
                            <td>
                         @if($user->status == 'Active')
                               <label class="badge badge-success">Active</label>
                          
                          @else
                                <label class="badge badge-danger">Pending</label>
                           @endif
                              
                            </td>
                             <!--<td>{{$user->role_id}}</td>-->
                            <td>
            
                             
                  
                  
                        @if(in_array("UserController@edit", $routeArray))
                              <a href="{{action('UserController@edit',$user->id)}}" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              @endif
                              @if(in_array("UserController@view", $routeArray))
                              <a href="{{action('UserController@view',$user->id)}}"  ><i class="fa fa-eye"></i> </a>
                             @endif  
                             @if(in_array("UserController@delete", $routeArray))
                             <a href="{{action('UserController@destroy', $user->id)}}"  ><i class="fa fa-trash" aria-hidden="true" title="Delete" Onclick="return ConfirmDelete();"></i></a>
                             @endif

        
                    
                              
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div></div>
    
@include('footer')