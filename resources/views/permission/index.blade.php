@include('header')

<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Permission</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="{{ url('permission/create') }}">Add Permission</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>                            
                                </li>
                                <li class="active">Permission </li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
            	<a href="{{ url('permission/create') }}" class="btn btn-info" role="button">Create Permission</a>

<br><br>
              <h4 class="card-title">Permission </h4>
              <div class="row">
                 @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                  @endif
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="example1" class="display" style="width:100%;">
                      <thead>
                        <tr>
                            <th>Sr.no.</th>
                            <th>Group Name</th>
                            <th>Name</th>
                            <th>Controller Name</th>
                            <th>Action Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php $i = 0
                         @endphp
                          @foreach($roles as $role)
                          @php $i++
                         @endphp
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$role->permission_name}}</td>
                            <td>{{$role->name}}</td>
                            <td>{{$role->controller_name}}</td>
                            <td>{{$role->action_name}}</td>
                            <td>
                         @if($role->status == '1')
                               <label class="badge badge-success">Active</label>
                          
                          @else
                                <label class="badge badge-danger">Pending</label>
                           @endif
                              
                        
                            </td>
                            <td>
                              <a href="{{action('PermissionController@edit',$role->id)}}" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@include('footer')