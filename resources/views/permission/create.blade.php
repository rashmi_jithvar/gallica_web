@include('header')
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Create Permission</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="{{ url('home') }}">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="{{ url('permission') }}"> Permission</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>                            
                                </li>
                                <li class="active">Add Permission </li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Permission </h4>
                          <form method="POST" action="{{ url('permission/store') }}">
                                            @csrf
                                            @if(Session::has('message'))
                                                    <div class='alert alert-success'>
                                                    {{ Session::get('message') }}
                                                    @php
                                                    Session::forget('message');
                                                    @endphp
                                                    </div>
                                            @endif
                                <div class="form-group">
                                      <label for="City">Choose GroupName</label>
                                      <select name="group_id"  class="form-control">
                                         <option disabled selected >Select Permission Group </option>
                                @if (isset($groupdropdown))
                                @foreach($groupdropdown as $key => $dropdownGroup)
                                    
                                      <option value ="{{$key}}">{{ $dropdownGroup }} </option>
                                      @endforeach
                                       @endif
                                    </select>
                                    <span class="text-danger">{{ $errors->first('group_id') }}</span> 
                                </div>            
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Name:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Name" name="name" >
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Controller Name:</label>
                                        <input type="text" class="form-control" id="roles" placeholder="Enter Controller Name" name="controller_name"  >
                                        <span class="text-danger">{{ $errors->first('controller_name') }}</span>
                                    </div>
                                </div>
                            <div class="form-group row">
                
                                    <div class="col">
                                        <label>Action Name:</label>
                                        <input type="text" class="form-control" id="roles" placeholder="Enter Action name" name="action_name"  >
                                        <span class="text-danger">{{ $errors->first('action_name') }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control"  name="status" required>
                                            <option disabled selected >Select Status</option>
                                            <option value="1" >Active</option>
                                            <option value="0" >Inactive</option>
                                        </select>
                                        <span class="text-danger">{{ $errors->first('status') }}</span>
                                    </div>

                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>

@include('footer')