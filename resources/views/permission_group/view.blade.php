@include('header')
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Warehouse </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            
                                </li>
                                <li class="active">Warehouse</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
            	<a href="{{ url('warehouse/create') }}" class="btn btn-info" role="button">Create Warehouse</a>

<br><br>
              <h4 class="card-title">Warehouse </h4>
                  <div class="container">
                    <div class="row">
                      <div class="col">
                        1 of 2
                      </div>
                      <div class="col">
                        2 of 2
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        1 of 3
                      </div>
                      <div class="col">
                        2 of 3
                      </div>
                      <div class="col">
                        3 of 3
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
@include('footer')