<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Role Permission </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            
                                </li>
                                <li class="active">Role Permission</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
            	<a href="<?php echo e(url('role_permission/create')); ?>" class="btn btn-info" role="button">Create Role Permission</a>

<br><br>
              <h4 class="card-title">Role Permission </h4>
              <div class="row">

                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>S.no. #</th>
                            <th>Permission </th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($role->id); ?></td>
                            <td><?php echo e($role->permission_id); ?></td>
                            <td><?php echo e($role->role); ?></td>
                            <td>
                         <?php if($role->status == '1'): ?>
                               <label class="badge badge-success">Active</label>
                          
                          <?php else: ?>
                                <label class="badge badge-danger">Pending</label>
                           <?php endif; ?>
                              
                        
                            </td>
                            <td>
            
                    <form action="<?php echo e(action('RolePermissionController@destroy', $role->id)); ?>" method="post">
                    <?php echo e(csrf_field()); ?>

                        <input name="_method" type="hidden" value="DELETE">
                              <a href="<?php echo e(action('RolePermissionController@edit',$role->id)); ?>" class="btn btn-info" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <button class="btn btn-danger" type="submit"><i class="fa fa-trash" ></i></button>
                    </form>
                              
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\finance_laravel\finance\resources\views/role_permission/index.blade.php ENDPATH**/ ?>