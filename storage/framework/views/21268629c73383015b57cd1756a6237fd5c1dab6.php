<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
    <div class="page-content" style="min-height:1271px">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Ticket </div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <?php if(in_array("TicketController@create", $routeArray)): ?>
                    <li><a class="parent-item" href="<?php echo e(url('ticket/create')); ?>">Add Ticket</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <?php endif; ?>
                    <li class="active">Ticket</li>
                </ol>
            </div>
        </div>
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="card">

                    <div class="card-body">
            <?php if(in_array("TicketController@create", $routeArray)): ?>
                <a href="<?php echo e(url('ticket/create')); ?>" class="btn btn-info" role="button">Create Ticket</a>
              <?php endif; ?>
                       

                        <br>
                        <br>
                        <h4 class="card-title">Ticket</h4>
                        <div class="row">
                 <?php if(session()->has('message')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session()->get('message')); ?>

                                </div>
                  <?php endif; ?>
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table id="order-listing" class="table">
                                        <thead>
                                            <tr>
                                                <th>Sr.no.</th>
                                                <th>Remark </th>
                                                <th>User</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                                <!--<th>Role Id</th>-->
                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 0 ?> <?php $__currentLoopData = $ticket; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php $i++ ?>
                                            <tr>
                                                <td><?php echo e($i); ?></td>
                                                <td><?php echo e($user->remark); ?></td>

                                                <td><?php echo e($user->user_id); ?></td>
                                                <td>
                                                    <?php if($user->status == '1'): ?>
                                                    <label class="badge badge-success">Active</label>

                                                    <?php else: ?>
                                                    <label class="badge badge-danger">Pending</label>
                                                    <?php endif; ?>

                                                </td>
                                               
                                                <td>

                                        
                                                    <?php if(in_array("TicketController@edit", $routeArray)): ?>
                                                        <a href="<?php echo e(action('TicketController@edit',$user->id)); ?>" title="update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                      <?php endif; ?>
                                                   <?php if(in_array("TicketController@delete", $routeArray)): ?>
                                                      <a href="<?php echo e(action('TicketController@destroy', $user->id)); ?>"  ><i class="fa fa-trash" aria-hidden="true" title="Delete" Onclick="return ConfirmDelete();"></i></a> 
                                                        <?php endif; ?>
                                                 

                                                </td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ct/public_html/finance/resources/views/ticket/index.blade.php ENDPATH**/ ?>