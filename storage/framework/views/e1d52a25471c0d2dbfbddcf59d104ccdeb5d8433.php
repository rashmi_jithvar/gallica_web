<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">View Client</div>
                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('client')); ?>"> Client</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">View Client</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
                      <div class="col-sm-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header>View Client</header>
                                 </div>
                                 <div class="card-body ">
                                 <div class="table-scrollable">
                                  <table id="mainTable" class="table table-striped">
                                  <thead>

                                  </thead>
                                  <tbody>
                                      <tr>
                                          <th>First Name</th>
                                          <td><?php echo e($user->first_name); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Last Name</th>
                                          <td><?php echo e($user->last_name); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Email</th>
                                          <td><?php echo e($user->email); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Contact</th>
                                          <td><?php echo e($user->contact); ?></td>
                                      </tr>                                      
                                      <tr>
                                          <th>Address Line 1</th>
                                          <td><?php echo e($user->address_line_1); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Address Line 2</th>
                                          <td><?php echo e($user->address_line_2); ?></td>
                                      </tr>                                      
                                      <tr>
                                          <th>City</th>
                                          <td><?php echo e($user->city); ?></td>
                                      </tr>
                                      <tr>
                                          <th>State</th>
                                          <td><?php echo e($user->state); ?></td>
                                      </tr>                                     
                                       <tr>
                                          <th>Pincode</th>
                                          <td><?php echo e($user->pincode); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Company Name</th>
                                          <td><?php echo e($user->company_name); ?></td>
                                      </tr>                                     
                                       <tr>
                                          <th>Website</th>
                                          <td><?php echo e($user->website); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Gst No</th>
                                          <td><?php echo e($user->gst_number); ?></td>
                                      </tr>  
                                      <tr>
                                          <th>Credit Period</th>
                                          <td><?php echo e($user->credit_period); ?></td>
                                      </tr>                                        
                                      <tr>
                                          <th>Credit Period Defination</th>
                                          <td><?php if($user->credit_period_defination=='1'): ?>
                                                   Calculation from billing date
                                            <?php endif; ?>
                                            <?php if($user->credit_period_defination=='2'): ?>
                                                 Calculation from Acceptance
                                            <?php endif; ?>
                                            <?php if($user->credit_period_defination=='3'): ?>
                                              Calculation from Finance
                                            <?php endif; ?></td>
                                      </tr>                                        
                                      <tr>
                                          <th>SPOC Business Developement</th>
                                          <td><?php echo e($role_ops->name); ?></td>
                                      </tr>                                       
                                       <tr>
                                          <th>SPOC OPS</th>
                                          <td><?php echo e($role_business->name); ?></td>
                                      </tr>                                        
                                      <tr>
                                          <th>SPOC Finance</th>
                                          <td><?php echo e($role_finance->name); ?></td>
                                      </tr> 
                                        <tr>
                                        <th>Client Warehouse</th>
                                          <td>
                                       
                                        <?php $__currentLoopData = $client_warehouse; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $warehouse): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php echo e($warehouse->name); ?> <br>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       
                                        </td>
                                      </tr>
                                                                             
                                      <tr>
                                          <th>Logo</th>
                                          <td>
                                              <?php if($user->image !=''): ?><img src="public/images/<?php echo e($user->image); ?>" width="80"><?php endif; ?></td>
                                      </tr>                                        
                                    <tr>
                                          <th>Status</th>
                                      <td>                           
                                             <?php if($user->status == '1'): ?>
                                                   <label class="badge badge-success">Active</label>
                                              
                                              <?php else: ?>
                                                    <label class="badge badge-danger">Pending</label>
                                               <?php endif; ?>
                                      </td>
                                      </tr>
                                       <tr>
                                          <th>Created by</th>
                                          <td><?php echo e($user->created_by); ?></td>
                                      </tr>                                                                             
                                      <tr>
                                          <th>Created At</th>
                                          <td><?php echo e(date("d-m-Y",strtotime($user->created_at))); ?></td>
                                      </tr>                                       
                                  </tbody>
                                  <tfoot>
                                  </tfoot>
                              </table>
                              </div>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>
            </div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <?php /**PATH /home/ptindiao/public_html/demo/finance/resources/views/client/view.blade.php ENDPATH**/ ?>