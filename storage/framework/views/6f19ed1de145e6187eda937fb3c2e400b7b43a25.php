<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    

    <script type="text/javascript">

      google.charts.load('current', {'packages':['corechart']});

      google.charts.setOnLoadCallback(drawChart);



      function drawChart() {

        var data = google.visualization.arrayToDataTable([

          ['Month', 'Pending-Invocie', 'Approved-Invoice'],

          ['Jan',  <?= $pending[0];?>,      <?= $approve[0];?>],

          ['Feb',  <?= $pending[1];?>,      <?= $approve[1];?>],

          ['March', <?= $pending[2];?>,       <?= $approve[2];?>],

          ['April', <?= $pending[3];?>,      <?= $approve[3];?>],

          ['May',  <?= $pending[4];?>,      <?= $approve[4];?>],

          ['June',  <?= $pending[5];?>,      <?= $approve[5];?>],

          ['July',  <?= $pending[6];?>,      <?= $approve[6];?>],

          ['August', <?= $pending[7];?>,      <?= $approve[7];?>],

          ['Sep',  <?= $pending[8];?>,      <?= $approve[8];?>],

          ['Oct',  <?= $pending[9];?>,      <?= $approve[9];?>],

          ['Nov',  <?= $pending[10];?>,      <?= $approve[10];?>],

          ['Dec',  <?= $pending[11];?>,      <?= $approve[11];?>],

        ]);



        var options = {

          title: 'Invoice Status',

          curveType: 'function',

          legend: { position: 'bottom' }

        };



        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));



        chart.draw(data, options);

      }

    </script>

    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Due"s', 'No of client'],
          ['1.5 cr',     <?php if(isset($one_five_cr)) { echo count($one_five_cr); }?>],
          ['2 cr',      <?php if(isset($two_cr)) { echo count($two_cr); }?>],
          ['> 2 cr',  <?php if(isset($two_five_cr)) { echo count($two_five_cr); }?>],

        ]);

        var options = {
          title: 'My Daily Activities'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }

    </script>

  <!-- Content Wrapper. Contains page content -->

 <div class="page-content-wrapper">

                <div class="page-content">

                    <div class="page-bar">

                        <div class="page-title-breadcrumb">

                            <div class=" pull-left">

                                <div class="page-title">Dashboard</div>

                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">

                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li class="active">Dashboard</li>

                            </ol>

                        </div>

                    </div>

                   <!-- start widget -->

          <div class="state-overview">

              <div class="row">

                    <div class="col-xl-3 col-md-6 col-12">

                      <div class="info-box bg-b-orange">

                        <span class="info-box-icon push-bottom"><i class="material-icons">shopping_cart</i></span>

                        <div class="info-box-content">

                          <span class="info-box-text">Total Invoice</span>

                          <span class="info-box-number"><?php echo e($total_invoice); ?></span>

                         <div class="progress">

                            <div class="progress-bar" style="width: <?php echo e($total_invoice); ?>%"><?php echo e($total_invoice); ?></div>

                          </div>

                          <span class="progress-description">
                            Total Invoice <?php echo e($total_invoice); ?>%
                          </span>

                        </div>

                        <!-- /.info-box-content -->

                      </div>

                      <!-- /.info-box -->

                    </div>

                    <!-- /.col -->

                    <div class="col-xl-3 col-md-6 col-12">

                      <div class="info-box bg-b-purple">

                        <span class="info-box-icon push-bottom"><i class="material-icons">redeem</i></span>

                        <div class="info-box-content">

                          <span class="info-box-text">Pending Invoice</span>

                          <span class="info-box-number"><?php echo e($pending_invoice); ?></span>

                         <div class="progress">

                            <div class="progress-bar" style="width: <?php echo e($pending_invoice); ?>%"><?php echo e($pending_invoice); ?></div>

                          </div>


                          <span class="progress-description">
                            Pending Invoice <?php echo e($pending_invoice); ?>%
                              </span>

                        </div>

                        <!-- /.info-box-content -->

                      </div>

                      <!-- /.info-box -->

                    </div>

                    <!-- /.col -->

                    <div class="col-xl-3 col-md-6 col-12">

                      <div class="info-box bg-b-cyan">

                        <span class="info-box-icon push-bottom"><i class="material-icons">group</i></span>

                        <div class="info-box-content">

                          <span class="info-box-text">Approved</span>

                          <span class="info-box-number"><?php echo e($approve_invoice); ?></span>

                          <div class="progress">

                            <div class="progress-bar" style="width: <?php echo e($approve_invoice); ?>%"><?php echo e($approve_invoice); ?></div>

                          </div>

                          <span class="progress-description">
                            Approved Invoice <?php echo e($approve_invoice); ?>%
                              </span>

                        </div>

                        <!-- /.info-box-content -->

                      </div>

                      <!-- /.info-box -->

                    </div>

                    <!-- /.col -->



                    <!-- /.col -->

                  </div>
<?php if($name = Auth::user()->role_id =='1'): ?>
            <div class="row">

                    <div class="col-sm-8">

                       <div class="card card-box">

                           <div class="container">

                        

                                <div id="curve_chart" style=""></div>

                        </div>

                      </div>

                  </div>

                   <div class="col-sm-4">

                        <div class="">

                           <div class="container">

                    <div id="piechart" style=""></div>

                  </div>

                </div>

                   </div>

              </div>
    <?php endif; ?>          

          </div>

      </div>

  </div>



<!-- ./wrapper -->



<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ct/public_html/finance/resources/views/home1.blade.php ENDPATH**/ ?>