<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>Login</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="<?php echo e(url('/public/admin')); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo e(url('/public/admin')); ?>/fonts/material-design-icons/material-icon.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap -->
  <link href="<?php echo e(url('/public/admin')); ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="<?php echo e(url('/public/admin')); ?>/assets/css/pages/extra_pages.css">
  <!-- favicon -->
    <link rel="shortcut icon" href="<?php echo e(url('/public/admin')); ?>/assets/img/favicon.ico" /> 
</head>
<body>
    <div id="app">

        <main class="py-4">
            <?php echo $__env->yieldContent('content'); ?>
        </main>
    </div>
    <script src="<?php echo e(url('/public/admin')); ?>/assets/plugins/jquery/jquery.min.js" ></script>
    <script src="<?php echo e(url('/public/admin')); ?>/assets/js/pages/extra_pages/pages.js" ></script>
</body>
</html>
<?php /**PATH D:\wamp\www\laravel_books\resources\views/layouts/app.blade.php ENDPATH**/ ?>