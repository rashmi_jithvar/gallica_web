<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add User</h4>
                          <form method="POST" action="<?php echo e(url('permissiongroup/store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>FullName:</label>
                                        <input type="text" class="form-control" id="roles" placeholder="Enter Fullname" name="name" >
                                        
                                    </div>
                                    <div class="col">
                                        <label>Email:</label>
                                        <input type="text" class="form-control" id="roles" placeholder="Enter Email" name="Email" >
                                        
                                    </div>
                                    <div class="col">
                                        <label for="exampleSelectGender">Role Id</label>
                                        <select class="form-control" id="exampleSelectGender" name="role_id" required>
                                            <option>Select Role Id</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col">
                                        <label>Username:</label>
                                        <input type="text" class="form-control" id="roles" placeholder="Enter Username" name="username" >
                                        
                                    </div>
                                <div class="col">
                                        <label>Password:</label>
                                        <input type="text" class="form-control" id="password" placeholder="Enter Password" name="password" >
                                        
                                </div>
                                <div class="col">
                                        <label>Confirm Password:</label>
                                        <input type="text" class="form-control" id="confirm_password" placeholder="Enter Confirm Password" name="confirm_password" >
                                        
                                </div>

                              </div>
                              <div class="form-group row">
                                    <div class="col-md-4">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control" id="exampleSelectGender" name="status" required>
                                            <option>Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>

                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\laravel\resources\views/user/create.blade.php ENDPATH**/ ?>