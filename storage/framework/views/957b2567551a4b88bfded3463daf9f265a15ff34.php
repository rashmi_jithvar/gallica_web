<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.urbanui.com/fily/template/demo/vertical-icon-menu/pages/samples/login-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Jan 2019 20:54:19 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Fily Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo e(url('/public/b')); ?>/vendors/iconfonts/mdi/font/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo e(url('/public/b')); ?>/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo e(url('/public/b')); ?>/vendors/css/vendor.bundle.addons.css">
    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.6.95/css/materialdesignicons.min.css
">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo e(url('/public/b')); ?>/css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo e(url('/public/b')); ?>/images/favicon.png" />
</head>
<body>
    <div id="app">

        <main class="py-4">
            <?php echo $__env->yieldContent('content'); ?>
        </main>
    </div>
</body>
</html>
<?php /**PATH D:\wamp\www\laravel\resources\views/layouts/app.blade.php ENDPATH**/ ?>