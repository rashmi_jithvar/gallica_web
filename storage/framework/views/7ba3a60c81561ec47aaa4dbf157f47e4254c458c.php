<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Warehouse </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                             <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('warehouse')); ?>"> Warehouse</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"> View Warehouse</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
                      <div class="col-sm-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header>View Client</header>
                                 </div>
                                 <div class="card-body ">
                                 <div class="table-scrollable">
                                  <table id="mainTable" class="table table-striped">
                                  <thead>

                                  </thead>
                                  <tbody>
                                      <tr>
                                          <th>Name</th>
                                          <td><?php echo e($user->name); ?></td>
                                      </tr>
                                      <tr>
                                          <th>Address</th>
                                          <td><?php echo e($user->address); ?></td>
                                      </tr>
                                      <tr>
                                          <th>City</th>
                                          <td><?php echo e($user->city); ?></td>
                                      </tr>
                                      <tr>
                                          <th>State</th>
                                          <td><?php echo e($user->state); ?></td>
                                      </tr>                                      
                                      <tr>
                                          <th>Country</th>
                                          <td><?php echo e($user->country); ?></td>
                                      </tr>
                                        
                                        <tr>
                                          <th>Created by</th>
                                          <td><?php echo e($user->created_by); ?></td>
                                      </tr>                                                                             
                                      <tr>
                                          <th>Created At</th>
                                          <td><?php echo e($user->created_at); ?></td>
                                      </tr>                                       
                                  </tbody>
                                  <tfoot>
                                  </tfoot>
                              </table>
                              </div>
                                 </div>
                             </div>
                         </div>
                    </div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ptindiao/public_html/demo/finance/resources/views/warehouse/view.blade.php ENDPATH**/ ?>