<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Revenue</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <?php if(isset($routeArray)): ?>
                                <?php if(in_array("RevenueController@create", $routeArray)): ?>
                                <li><a class="parent-item" href="<?php echo e(url('revenue/create')); ?>">Add Revenue</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <?php endif; ?>
                                <?php endif; ?>
                                <li class="active">Revenue</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
          <?php if(isset($routeArray)): ?>
        <?php if(in_array("RevenueController@create", $routeArray)): ?>
            	<a href="<?php echo e(url('revenue/create')); ?>" class="btn btn-info" role="button">Create Revenue</a>
              <?php endif; ?>
              <?php endif; ?>


<br><br>
              <h4 class="card-title">Revenue</h4>
              <div class="row">

                <div class="col-12">                            
                  <?php if(session()->has('message')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session()->get('message')); ?>

                                </div>
                  <?php endif; ?>
                  <div class="table-responsive">
                    <table id="example1" class="display" style="width:100%;">
                      <thead>
                        <tr>
                            <th>Sr.no.</th>
                            <th>Name</th>
                            <th>Status</th>
                            
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 0
                         ?>
                        <?php $__currentLoopData = $revenue; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++
                         ?>
                        <tr>
                            <td><?php echo e($i); ?></td>
                            <td><?php echo e($role->name); ?></td>
                            <td>
                         <?php if($role->status == '1'): ?>
                               <label class="badge badge-success">Active</label>
                          
                          <?php else: ?>
                                <label class="badge badge-danger">Pending</label>
                           <?php endif; ?>
                              
                        
                            </td>
                            <td>
            
                  
                        <input name="_method" type="hidden" value="DELETE">
                         <?php if(isset($routeArray)): ?>
                        <?php if(in_array("RevenueController@edit", $routeArray)): ?>
                              <a href="<?php echo e(action('RevenueController@edit',$role->id)); ?>" title="update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <?php endif; ?>
                            <?php endif; ?>
                       <?php if(isset($routeArray)): ?>     
                        <?php if(in_array("RevenueController@delete", $routeArray)): ?>
                        <a href="<?php echo e(action('RevenueController@destroy', $role->id)); ?>"  ><i class="fa fa-trash" aria-hidden="true" title="Delete" Onclick="return ConfirmDelete();"></i></a>
                      <?php endif; ?>
                        <?php endif; ?>
              
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ptindiao/public_html/demo/finance/resources/views/revenue/index.blade.php ENDPATH**/ ?>