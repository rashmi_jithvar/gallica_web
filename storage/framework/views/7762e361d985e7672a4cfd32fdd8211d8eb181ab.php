<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('home')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
	<div class="col-md-12">
	
	<section class="content">
      <div class="row">		
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">All Members </h3>
              <a href="<?php echo e(url('create')); ?>" class="btn btn-success pull-right">Add Member</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
            <?php if($errors->any()): ?>
                <div class="alert alert-danger" style="margin-top:15px;">
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            
            <?php if(Session::has('message')): ?>
                <div class='alert alert-success' style="margin-top:15px;">
                <?php echo e(Session::get('message')); ?>

                <?php
                    Session::forget('message');
                ?>
                </div>
            <?php endif; ?>
                
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>User Type</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr> 
                    <td><?php echo e($user->name); ?></td>
                    <td><?php echo e($user->email); ?></td>
                    <td><?php echo e($user->status); ?></td>
                    <td><?php echo e($user->user_type); ?></td>
                    <td>
                        <a href="#" class="btn-delete" data-id="<?php echo e($user->id); ?>">Delete</a>
                        <a href="<?php echo e(url('edit/'.$user->id)); ?>">Edit</a>
                    </td>
                </tr> 
				          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
</section>
	</div>
</div>


<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script>
$('#example1').dataTable({
    'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': [-1], /* 1st colomn, starting from the right */
    }]
});



$('.btn-delete').click(function(){
    var id = $(this).attr('data-id');
   if(confirm("Are you sure want to delete ?")){
       window.location.href="<?php echo e(url('member/delete')); ?>/"+id;
   }
    else{
        return false;
    }
});
</script><?php /**PATH D:\wamp\www\mcgill-laravel\resources\views/member/show.blade.php ENDPATH**/ ?>