<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- Content Wrapper. Contains page content -->
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Change Password</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                               
                                </li>
                                <li class="active">Change Password</li>
                            </ol>
                        </div>
                    </div>
  <div class="main-panel">
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

      <div class="row">
        <!-- left column -->
        <div class="col-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
              <h3 class="box-title">Change Password</h3>
              
              <?php if($errors->any()): ?>
                <div class="alert alert-danger" style="margin-top:15px;">
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            
            <?php if(Session::has('message')): ?>
                <div class='alert alert-success' style="margin-top:15px;">
                <?php echo e(Session::get('message')); ?>

                <?php
                    Session::forget('message');
                ?>
                </div>
            <?php endif; ?>
            
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo e(url('updatepassword')); ?>" method="post">
              <input type="hidden" name="_token" id="csrf-token" value="<?php echo e(Session::token()); ?>" />
              <div class="box-body">
                <div class="form-group">
                  <div class="col">
                  <label for="exampleInputEmail1">New Password </label>
                  <input type="password" name="password" required="" class="form-control" placeholder="Enter Password">
                </div>
                </div>
                <div class="form-group">
                  <div class="col">
                  <label for="exampleInputEmail1">Confirm Password</label>
                  <input type="password" name="password_confirmation" required="" class="form-control" placeholder="Confirm password">
                </div>
                </div> 
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <div class="col">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              </div>
            </form>
          </div>
        </div>
          <!-- /.box -->
		</div> 
	</div>
</div>
</div>
</div>


<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\laravel_books\resources\views/profile/changepassword.blade.php ENDPATH**/ ?>