<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
  <style>
   .error{ color:red; } 
  </style>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Add Client </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('client')); ?>"> Client</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Add Client</li>
                            </ol>
                        </div>
                    </div>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Client</h4>
                          <form  id="customerform" method="POST" action="<?php echo e(url('import_list/csvfileupload')); ?>" enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label> Please Select File (CSV only)::<span class="required-mark">*</span></label>
                                        <input type="file" name="csvfile" class="form-control"  placeholder="Enter company name" >
                                         <span class="text-danger"><?php echo e($errors->first('csvfile')); ?></span> 
                                    </div>  
 
                              </div>
                              <div class="form-group row">
                
                                
                      
                              </div>
                              <div class="form-group row">
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                             </div>
                              </div>
   
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>
<script>
   if ($("#customerform").length > 0) {
    $("#customerform").validate({
     
    rules: {
    first_name: {
        required: true,
        maxlength: 150
      },
    company_name: {
        required: true,
        maxlength: 255
      },
    mobile: {
        required: true,
        maxlength: 10,
        number: true,

      },
    email: {
        required: true,
        email: true,
      },
    address_line_1: {
        required: true,
      },
   
    state: {
          required: true,
          maxlength: 150,
    }, 
    city: {
        required: true,
      },
   
    pincode: {
          required: true,
          maxlength: 6,
    }, 
    credit_period_defination: {
          required: true,
          maxlength: 50,
    }, 
    website: {  
        url: true
    },
    first_contact_spoc: {
          required: true,
          maxlength: 255,
    }, 
    warehouse_id: {
          required: true,
          
    }, 
   
  },
    messages: {
       
      first_name: {
        required: "Please Enter First name",
        maxlength: "Your last name maxlength should be 50 characters long."
      },

    company_name: {
        required: "Please Enter Company name",
        maxlength: "Your company_name should be 255 characters long."
      },
    mobile: {
        required: "Please Enter Mobile number",
        maxlength: "Your mobile should be 10 characters long."
      },  
    email: {
        required: "Please Enter Email address",
          email: "Please enter valid email",
        
      },
    address_line_1: {
        required: "Please Enter Address",
       
      }, 
    state: {
          required: "Please Enter state",
          
        },
    city: {
          required: "Please Enter City",
          
      },
    pincode: {
          required: "Please Enter pincode",
          
      },
    credit_period_defination: {
          required: "Please Enter credit_period_defination",
          
        },
    first_contact_spoc: {
          required: "Please Select First Contact SPOC",
          
        },
    website: {
          required: "Please Enter valid url",
          
        },
    warehouse_id: {
          required: "Please Select Warehouse",
          
        },    
   
       
    },
    })
  }
</script>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\urbanclap\resources\views/import/create.blade.php ENDPATH**/ ?>