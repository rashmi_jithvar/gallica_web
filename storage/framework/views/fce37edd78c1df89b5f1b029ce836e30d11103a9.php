<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Client </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                       
                            <li><i class="fa fa-plus"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('import_list/create')); ?>">Add Client</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                
                                <li class="active">Client</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
           
            	<a href="<?php echo e(url('import_list/create')); ?>" class="btn btn-info" role="button">Import CSv File</a>
            
            	

<br><br>
              <h4 class="card-title">List </h4>
              <div class="row">
                 <?php if(session()->has('message')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session()->get('message')); ?>

                                </div>
                  <?php endif; ?>
                <div class="col-12">
                  <div class="table-responsive">

                    <button style="margin-bottom: 10px" class="btn btn-danger delete_all" data-url="<?php echo e(url('/import_list/bulkDelete')); ?>">Delete All Selected</button>
                    
                    <table id="example1" class="display" style="width:100%;">
                      <thead>
                        <tr>
                           <th><input type="checkbox" id="master"> &nbsp;Select </th>
                            <th>Sr.no.</th>
                            <th>Docket no. </th> 
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 0
                         ?>
                          <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ware): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php $i++
                        ?>
                        <tr>
                            <td><input type="checkbox" class="sub_chk" data-id="<?php echo e($ware->id ?? ''); ?>"></td>
                            <td><?php echo e($i); ?></td>
                            <td><?php echo e($ware->docket ? $ware->docket:''); ?></td>
                            
                           <!-- <td>
                              <img src="public/images/<?php echo e($ware->image); ?>" width="80"></td>-->
                            <td>
                         <?php if($ware->status == 'Delivered'): ?>
                               <label class="badge badge-success">Delivered</label>
                          
                          <?php elseif($ware->status == 'IN TRANSIT'): ?>
                                <label class="badge badge-info">IN TRANSIT</label>
                          <?php else: ?>
                                 <label class="badge badge-primary">In Active</label>
                          <?php endif; ?>

                              
                            </td>
                            <td>
            
                        
                              <a href="<?php echo e(action('ImportController@show',$ware->id)); ?>" title="view"><i class="fa fa-eye" aria-hidden="true"></i></a>
                         
                         <a href="<?php echo e(action('ImportController@destroy', $ware->id)); ?>"  ><i class="fa fa-trash" aria-hidden="true" title="Delete" Onclick="return ConfirmDelete();"></i></a>
                      
              
                              
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
    $(document).ready(function () {


        $('#master').on('click', function(e) {
         if($(this).is(':checked',true))  
         {
            $(".sub_chk").prop('checked', true);  
         } else {  
            $(".sub_chk").prop('checked',false);  
         }  
        });


        $('.delete_all').on('click', function(e) {


            var allVals = [];  
            $(".sub_chk:checked").each(function() {  
                allVals.push($(this).attr('data-id'));
            });  


            if(allVals.length <=0)  
            {  
                alert("Please select row.");  
            }  else {  


                var check = confirm("Are you sure you want to delete this row?");  
                if(check == true){  


                    var join_selected_values = allVals.join(","); 


                    $.ajax({
                        url: $(this).data('url'),
                        type: 'DELETE',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+join_selected_values,
                        success: function (data) {
                            if (data['success']) {
                                $(".sub_chk:checked").each(function() {  
                                    $(this).parents("tr").remove();
                                });
                                alert(data['success']);
                            } else if (data['error']) {
                                alert(data['error']);
                            } else {
                                alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });


                  $.each(allVals, function( index, value ) {
                      $('table tr').filter("[data-row-id='" + value + "']").remove();
                  });
                }  
            }  
        });


        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            onConfirm: function (event, element) {
                element.trigger('confirm');
            }
        });


        $(document).on('confirm', function (e) {
            var ele = e.target;
            e.preventDefault();


            $.ajax({
                url: ele.href,
                type: 'DELETE',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (data) {
                    if (data['success']) {
                        $("#" + data['tr']).slideUp("slow");
                        alert(data['success']);
                    } else if (data['error']) {
                        alert(data['error']);
                    } else {
                        alert('Whoops Something went wrong!!');
                    }
                },
                error: function (data) {
                    alert(data.responseText);
                }
            });


            return false;
        });
    });
</script><?php /**PATH D:\wamp\www\urbanclap\resources\views/import/index.blade.php ENDPATH**/ ?>