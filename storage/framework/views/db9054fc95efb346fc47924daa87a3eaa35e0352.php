<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
            	<a href="<?php echo e(url('permission_group/create')); ?>" class="btn badge badge-success" role="button">Create Roles</a>

<br><br>
              <h4 class="card-title">Permission Group</h4>
              <div class="row">

                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>S.no. #</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($role->id); ?></td>
                            <td><?php echo e($role->name); ?></td>
                            <td>
                         <?php if($role->status == '1'): ?>
                               <label class="badge badge-success">Active</label>
                          
                          <?php else: ?>
                                <label class="badge badge-danger">Pending</label>
                           <?php endif; ?>
                              
                        
                            </td>
                            <td>
            
                    <form action="<?php echo e(action('PermissionGroupController@destroy', $role->id)); ?>" method="post">
                    <?php echo e(csrf_field()); ?>

                        <input name="_method" type="hidden" value="DELETE">
                              <a href="<?php echo e(action('PermissionGroupController@edit',$role->id)); ?>" class="btn badge badge-success" role="button">Edit</a>
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                              
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\laravel\resources\views/permission_group/index.blade.php ENDPATH**/ ?>