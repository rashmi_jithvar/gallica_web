<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Client </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                        <?php if(in_array("ClientController@create", $routeArray)): ?>
                            <li><i class="fa fa-plus"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('client/create')); ?>">Add Client</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <?php endif; ?>
                                <li class="active">Client</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
            <?php if(in_array("ClientController@create", $routeArray)): ?>
            	<a href="<?php echo e(url('client/create')); ?>" class="btn btn-info" role="button">Create Client</a>
              <?php endif; ?>
            	

<br><br>
              <h4 class="card-title">Client </h4>
              <div class="row">
                 <?php if(session()->has('message')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session()->get('message')); ?>

                                </div>
                  <?php endif; ?>
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="example1" class="display" style="width:100%;">
                      <thead>
                        <tr>
                            <th>Sr.no.</th>
                            <th>Name </th>
                            <th>Company Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            
                            
                            <th>City</th>
                            <th>State</th>
                            
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 0
                         ?>
                          <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ware): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php $i++
                        ?>
                        <tr>
                            <td><?php echo e($i); ?></td>
                            <td><?php echo e($ware->first_name); ?>&nbsp;<?php echo e($ware->last_name); ?></td>
                           <td><?php echo e($ware->company_name); ?></td>
                           <td><?php echo e($ware->mobile); ?></td>
                           <td><?php echo e($ware->email); ?></td>
                            
                           
                            <td><?php echo e($ware->city); ?></td>
                            <td><?php echo e($ware->state); ?></td>
                            
                           <!-- <td>
                              <img src="public/images/<?php echo e($ware->image); ?>" width="80"></td>-->
                            <td>
                         <?php if($ware->status == '1'): ?>
                               <label class="badge badge-success">Active</label>
                          
                          <?php else: ?>
                                <label class="badge badge-danger">Pending</label>
                           <?php endif; ?>
                              
                            </td>
                            <td>
            
                             
               
                        <?php if(in_array("ClientController@edit", $routeArray)): ?>
                              <a href="<?php echo e(action('ClientController@edit',$ware->id)); ?>" title="update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <?php endif; ?>
                         <?php if(in_array("ClientController@view", $routeArray)): ?>
                              <a href="<?php echo e(action('ClientController@view',$ware->id)); ?>" title="view"><i class="fa fa-eye" aria-hidden="true"></i></a>
                         <?php endif; ?> 
                         <?php if(in_array("ClientController@delete", $routeArray)): ?>
                         <a href="<?php echo e(action('ClientController@destroy', $ware->id)); ?>"  ><i class="fa fa-trash" aria-hidden="true" title="Delete" Onclick="return ConfirmDelete();"></i></a>
                        <?php endif; ?>
              
                              
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ct/public_html/finance/resources/views/client/index.blade.php ENDPATH**/ ?>