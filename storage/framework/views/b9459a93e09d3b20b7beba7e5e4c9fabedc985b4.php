<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>

  <style>

   .error{ color:red; } 

  </style>

<div class="page-content-wrapper">

                <div class="page-content" style="min-height:1271px">

                    <div class="page-bar">

                        <div class="page-title-breadcrumb">

                            <div class=" pull-left">

                                <div class="page-title">Add CSV File Upload </div>

                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">

                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                            <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('import_list')); ?>"> List</a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li class="active">Add CSV File</li>

                            </ol>

                        </div>

                    </div>

<div class="main-panel">

    <div class="content-wrapper">

       

            <div class="row">



                <div class="col-12 grid-margin stretch-card">

                    <div class="card">

                        <div class="card-body">

                            <h4 class="card-title">Upload CSV File</h4>

                          <form  id="customerform" method="POST" action="<?php echo e(url('import_list/csvfileupload')); ?>" enctype="multipart/form-data">

                                            <?php echo csrf_field(); ?>

                                            <?php if(Session::has('message')): ?>

                                                    <div class='alert alert-success'>

                                                    <?php echo e(Session::get('message')); ?>


                                                    <?php

                                                    Session::forget('message');

                                                    ?>

                                                    </div>

                                            <?php endif; ?>

                                <div class="form-group row">

                

                                    <div class="col">

                                        <label> Please Select File (CSV only)::<span class="required-mark">*</span></label>

                                        <input type="file" name="csvfile" class="form-control"  placeholder="Enter company name" >

                                         <span class="text-danger"><?php echo e($errors->first('csvfile')); ?></span> 

                                    </div>  

 

                              </div>

                              <div class="form-group row">

                

                                

                      

                              </div>

                              <div class="form-group row">

                               <button type="submit" class="btn btn-primary mr-2">Submit</button>

                             </div>

                              </div>

   

                            </form>

                        </div>

                    </div>

                </div>



            </div>

    </div>

</div>

</div>

</div>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\backup_urbanclap\urbanclap\resources\views/import/create.blade.php ENDPATH**/ ?>