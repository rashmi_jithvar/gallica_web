 <?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Create Role Permission </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                             
                                </li>
                                <li class="active"> Add Role Permission</li>
                            </ol>
                        </div>
                    </div>
  <?php if(isset($role)): ?>  
  <?php endif; ?>                
  <?php if(isset($rolepermission)): ?>                  
  <?php if($rolepermission): ?>
  
        <?php $__currentLoopData = $rolepermission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $perKey => $perm_val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
            <?php $perm_array[] = $perm_val->permission_id;
            ?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

  <?php endif; ?>
  <?php endif; ?>    
                   
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body" >
                            <h4 class="card-title">Add Role Permission </h4>
                          <form method="POST" action="<?php echo e(url('role_permission/store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                              <div class="col-4">              
                                <div class="form-group">
                                      <label for="">Role</label>

                                      <select name="role_id" id="role_id" class="form-control" onchange="loadRole(this.value)">
                                         <option disabled selected>Select Role</option>
                                        <?php if(isset($roledropdown)): ?>
                                      <?php $__currentLoopData = $roledropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value ="<?php echo e($key); ?>" <?php echo e($role == $key ? 'selected':''); ?>><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                     <span class="text-danger"><?php echo e($errors->first('role_id')); ?></span> 

                                </div> 
                            </div>   
           
                          <?php if(isset($pergroup)): ?>
                            <?php $__currentLoopData = $pergroup; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <div class="row" >
                                <div class="col-md-12 m-b-20">  
                              <?php if($name->permission_name): ?> 
                                <h4><u><?php echo e($name->permission_name); ?> </u></h4>
                                <br>
                                <div class="row" >
                                  <?php $__currentLoopData = $name->permission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <?php $data_checked = ''
                                      ?>
                                         <?php if(isset($perm_array)): ?>
                                        <?php if($perm_array): ?>
                                        
                                          <?php if(in_array($permission->id, $perm_array)): ?>
                                            <?php $data_checked = "checked";
                                            ?>
                                          <?php endif; ?>
                                       <?php endif; ?>
                                    <?php endif; ?>       
                                  <div class="col-md-2"> 
                                  <label> <input type="checkbox" name="permission_id[]" value="<?php echo e($permission->id); ?>" <?php echo e($data_checked); ?>>

                                    <?php echo e($permission->name); ?></label>
                                  </div>
                                  
                          
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                              </div>

                              <?php endif; ?>
                        </div>
                      </div>
                      <hr/>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php endif; ?>

                              <!--  <div class="form-group row">
                                    <div class="col-4">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control" id="status" name="status" required>
                                            <option disabled selected>Select Status</option>
                                            <option value="1" >Active</option>
                                            <option value="0" >Inactive</option>
                                        </select>
                                    </div>
<span class="text-danger"><?php echo e($errors->first('status')); ?></span> 
                                </div>-->
                                
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
  </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript">
    function loadRole(value)
    {
     
        window.location = "<?php echo e(url('role_permission/create')); ?>?role="+value;
    }
</script>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\urbanclap\resources\views/role_permission/create.blade.php ENDPATH**/ ?>