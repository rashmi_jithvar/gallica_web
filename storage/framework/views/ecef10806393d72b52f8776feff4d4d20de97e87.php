<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <!-- Content Wrapper. Contains page content -->

 <div class="page-content-wrapper">

                <div class="page-content">

                    <div class="page-bar">

                        <div class="page-title-breadcrumb">

                            <div class=" pull-left">

                                <div class="page-title">Dashboard</div>

                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">

                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li class="active">Dashboard</li>

                            </ol>

                        </div>

                    </div>

                   <!-- start widget -->

          <div class="state-overview">

              <div class="row">

                    <div class="col-xl-3 col-md-6 col-12">

                      <div class="info-box bg-b-orange">

                        <span class="info-box-icon push-bottom"><i class="material-icons">shopping_cart</i></span>

                        <div class="info-box-content">

                          <span class="info-box-text"></span>

                          <span class="info-box-number"></span>

                         <div class="progress">

                            <div class="progress-bar" style="width:%"></div>

                          </div>

                          <span class="progress-description">
                            Total Invoice 
                          </span>

                        </div>

                        <!-- /.info-box-content -->

                      </div>

                      <!-- /.info-box -->

                    </div>

                    <!-- /.col -->

                    <div class="col-xl-3 col-md-6 col-12">

                      <div class="info-box bg-b-purple">

                        <span class="info-box-icon push-bottom"><i class="material-icons">redeem</i></span>

                        <div class="info-box-content">

                          <span class="info-box-text">Pending Invoice</span>

                          <span class="info-box-number"></span>

                         <div class="progress">

                            <div class="progress-bar" style="width: "></div>

                          </div>


                          <span class="progress-description">
                            Pending Invoice 
                              </span>

                        </div>

                        <!-- /.info-box-content -->

                      </div>

                      <!-- /.info-box -->

                    </div>

                    <!-- /.col -->

                    <div class="col-xl-3 col-md-6 col-12">

                      <div class="info-box bg-b-cyan">

                        <span class="info-box-icon push-bottom"><i class="material-icons">group</i></span>

                        <div class="info-box-content">

                          <span class="info-box-text">Approved</span>

                          <span class="info-box-number"></span>

                          <div class="progress">

                            <div class="progress-bar" style="width:"></div>

                          </div>

                          <span class="progress-description">
                            Approved Invoice 
                              </span>

                        </div>

                        <!-- /.info-box-content -->

                      </div>

                      <!-- /.info-box -->

                    </div>

                    <!-- /.col -->



                    <!-- /.col -->

                  </div>
          </div>

      </div>

  </div>



<!-- ./wrapper -->



<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\backup_urbanclap\urbanclap\resources\views/home1.blade.php ENDPATH**/ ?>