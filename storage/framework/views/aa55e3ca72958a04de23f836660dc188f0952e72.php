<!-- <?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> -->
<?php



?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Create Role Permission </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                            
                                </li>
                                <li class="active">Role Permission</li>
                            </ol>
                        </div>
                    </div>
  <?php if(isset($rolepermission)): ?>                  
  <?php if($rolepermission): ?>
        <?php $__currentLoopData = $rolepermission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $perKey => $perm_val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
            $perm_array[] = $perm_val->permission_id;
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
  <?php endif; ?>    
                   
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Role Permission </h4>
                          <form method="POST" action="<?php echo e(url('role_permission/store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                              <div class="col-4">              
                                <div class="form-group">
                                      <label for="">Role Id</label>
                                      <select name="role_id" class="form-control" onchange="loadRole(this.value)">
                                        <?php if(isset($roledropdown)): ?>
                                      <?php $__currentLoopData = $roledropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value ="<?php echo e($key); ?>" ><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                </div> 
                            </div>             
                          <?php if(isset($pergroup)): ?>
                            <?php $__currentLoopData = $pergroup; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php if($name->name): ?> 
                                <h4><u> <?php echo e($name->name); ?> </u></h4>
                                  <?php $__currentLoopData = $name->permission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <?php $data_checked = ''
                                      ?>
                                         <?php if(isset($perm_array)): ?>
                                        <?php if($perm_array): ?>
                                        
                                          <?php if(in_array($permission->id, $perm_array)): ?>
                                            <?php $data_checked = "checked";
                                            ?>
                                          <?php endif; ?>
                                       <?php endif; ?>
                                    <?php endif; ?>       
                                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                   <input type="checkbox" name="permission_id[]" value="<?php echo e($permission->id); ?>" <?php echo e($data_checked); ?>>
                                    &nbsp;&nbsp; <?php echo e($permission->name); ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php endif; ?>
                                <div class="form-group row">
                                    <div class="col-4">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control" id="exampleSelectGender" name="status" required>
                                            <option>Select Status</option>
                                            <option value="1" >Active</option>
                                            <option value="0" >Inactive</option>
                                        </select>
                                    </div>

                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
  </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript">
    function loadRole(value)
    {
      alert(value);
        window.location = "<?php echo e(url('role_permission/create')); ?>?role="+value;
    }
</script>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\finance_laravel\finance\resources\views/role_permission/create.blade.php ENDPATH**/ ?>