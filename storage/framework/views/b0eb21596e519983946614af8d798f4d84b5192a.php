<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="page-content-wrapper">
                <div class="page-content" style="min-height:1271px">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Company Wings </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <?php if(in_array("CompanyWingsController@create", $routeArray)): ?>
                            <li><i class="fa fa-plus"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('company/create')); ?>">Add Company Wings</a>&nbsp;<i class="fa fa-angle-right"></i>
                              <?php endif; ?>
                                </li>
                                <li class="active">Company Wings</li>
                            </ol>
                        </div>
                    </div>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">

            <div class="card-body">
              <a href="<?php echo e(url('company_wings/create')); ?>" class="btn btn-info" role="button">Create Company Wings</a>

<br><br>
              <h4 class="card-title">Company Wings </h4>
              <div class="row">

                <div class="col-12">
                  <div class="table-responsive">
                    <table id="example1" class="display" style="width:100%;">
                      <thead>
                        <tr>
                            <th>Sr.no.</th>
                            <th>Name </th>
                            <th>Company Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            
                            
                            <th>City</th>
                            <th>State</th>
                            
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 0
                         ?>
                          <?php $__currentLoopData = $company; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ware): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php $i++
                        ?>
                        <tr>
                            <td><?php echo e($i); ?></td>
                            <td><?php echo e($ware->first_name); ?>&nbsp;<?php echo e($ware->last_name); ?></td>
                           <td><?php echo e($ware->company_name); ?></td>
                           <td><?php echo e($ware->mobile); ?></td>
                           <td><?php echo e($ware->email); ?></td>
                            
                           
                            <td><?php echo e($ware->city); ?></td>
                            <td><?php echo e($ware->state); ?></td>
                            
                           <!-- <td>
                              <img src="public/images/<?php echo e($ware->image); ?>" width="80"></td>-->
                            <td>
                         <?php if($ware->status == '1'): ?>
                               <label class="badge badge-success">Active</label>
                          
                          <?php else: ?>
                                <label class="badge badge-danger">Pending</label>
                           <?php endif; ?>
                              
                            </td>
                            <td>
            
               <?php if(in_array("CompanyWingsController@edit", $routeArray)): ?>
                              <a href="<?php echo e(action('CompanyWingsController@edit',$ware->id)); ?>" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
               <?php endif; ?>   
                <?php if(in_array("CompanyWingsController@view", $routeArray)): ?>            
                              <a href="<?php echo e(action('CompanyWingsController@view',$ware->id)); ?>" title="view"><i class="fa fa-eye" aria-hidden="true"></i></a>
                <?php endif; ?>
                 <?php if(in_array("CompanyWingsController@delete", $routeArray)): ?>              
                           <a href="<?php echo e(action('CompanyWingsController@destroy', $ware->id)); ?>"  ><i class="fa fa-trash" aria-hidden="true" title="Delete" Onclick="return ConfirmDelete();"></i></a>  
                  <?php endif; ?>      
                
                              
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ptindiao/public_html/demo/finance/resources/views/company/index.blade.php ENDPATH**/ ?>