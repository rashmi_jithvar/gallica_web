<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Update Permission </h4>
                          <form method="POST" action="<?php echo e(action('PermissionController@update', $id)); ?>">
                                            <?php echo csrf_field(); ?>
                                            <?php if(Session::has('message')): ?>
                                                    <div class='alert alert-success'>
                                                    <?php echo e(Session::get('message')); ?>

                                                    <?php
                                                    Session::forget('message');
                                                    ?>
                                                    </div>
                                            <?php endif; ?>
                                <div class="form-group">
                                      <label for="City">Choose GroupName</label>
                                      <select name="dropdownCity" class="form-control">
                                        <?php if(isset($groupdropdown)): ?>
                                      <?php $__currentLoopData = $groupdropdown; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dropdownGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value ="<?php echo e($dropdownGroup->id); ?>"><?php echo e($dropdownGroup); ?> </option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       <?php endif; ?>
                                    </select>
                                </div>            
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Name:</label>
                                        <input type="text" class="form-control" id="roles" placeholder="Enter Roles" name="name" value="<?php echo e($permission->name); ?>" >
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Controller Name:</label>
                                        <input type="text" class="form-control" id="roles" placeholder="Enter Roles" name="name" value="<?php echo e($permission->controller_name); ?>" >
                                        
                                    </div>
                                </div>
                            <div class="form-group row">
                
                                    <div class="col">
                                        <label>Action Name:</label>
                                        <input type="text" class="form-control" id="roles" placeholder="Enter Roles" name="name" value="<?php echo e($permission->action_name); ?>" >
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control" id="exampleSelectGender" name="status" required>
                                            <option>Select Status</option>
                                            <option value="1" <?php if($permission->status=='1'): ?> selected='selected' <?php endif; ?>>Active</option>
                                            <option value="0" <?php echo e($permission->status == '0' ? 'selected':''); ?>>Inactive</option>
                                        </select>
                                    </div>

                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp\www\laravel\resources\views/permission/edit.blade.php ENDPATH**/ ?>