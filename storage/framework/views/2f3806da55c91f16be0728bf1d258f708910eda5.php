<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<style>
  .error{
    color:red;
  }
</style>
<div class="page-content-wrapper">
  <div class="page-content" style="min-height:1271px">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">Warehouse 
          </div>
        </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('home')); ?>">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                             <li><i class="fa fa-list"></i>&nbsp;<a class="parent-item" href="<?php echo e(url('warehouse')); ?>"> Warehouse</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"> Add Warehouse</li>
                            </ol>
    </div>
  </div>
  <div class="main-panel">
    <div class="content-wrapper">
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Add Warehouse
              </h4>
              <form  id="customerform" method="POST" action="<?php echo e(url('warehouse/store')); ?>">
                <?php echo csrf_field(); ?>
                <?php if(Session::has('message')): ?>
                <div class='alert alert-success'>
                  <?php echo e(Session::get('message')); ?>

                  <?php
                  Session::forget('message');
                  ?>
                </div>
                <?php endif; ?>
                <div class="form-group row">
                  <div class="col">
                    <label>Name:<span class="required-mark">*</span>
                    </label>
                    <input type="text"  id="formGroupExampleInput" class="form-control"  placeholder="Enter Warehouse Name" name="name" >
                    <span class="text-danger"><?php echo e($errors->first('name')); ?>

                    </span> 
                  </div>
                  <div class="col">
                    <label>Address:<span class="required-mark">*</span>
                    </label>
                    <input type="text" id="address" class="form-control"  placeholder="Enter Address" name="address" >
                    <span class="text-danger"><?php echo e($errors->first('address')); ?>

                    </span>
                  </div>
                  <div class="col">
                    <label for="exampleSelectGender">City<span class="required-mark">*</span>
                    </label>
                        <input type="text"  id="formGroupExampleInput" class="form-control"  placeholder="Enter City" name="city" >
                    <span class="text-danger"><?php echo e($errors->first('city')); ?></span>  
                  </div>
 
                </div>
                <div class="form-group row">
                  <!-- <div class="col">
<label>Username:</label>
<input type="text" class="form-control"  placeholder="Enter Username" name="username" >
<span class="text-danger"><?php echo e($errors->first('username')); ?></span>   
</div>-->
                  <div class="col">
                    <label for="exampleSelectGender">State<span class="required-mark">*</span>
                    </label>
                    <input type="text"  id="formGroupExampleInput" class="form-control"  placeholder="Enter State" name="state" >
                    <span class="text-danger"><?php echo e($errors->first('state')); ?></span>  
                  </div>
                  <div class="col">
                    <label for="exampleSelectGender">Country<span class="required-mark">*</span>
                    </label>
                        <input type="text"  id="formGroupExampleInput" class="form-control"  placeholder="Enter Country name" name="country" >
                    <span class="text-danger"><?php echo e($errors->first('country')); ?>

                    </span>  
                  </div>
                 <!-- <div class="col">
                    <label>Latitude
                    </label>
                    <input type="text" class="form-control" id="latitude" placeholder="Enter Latitude(Optional)" name="lat" >
                    <span class="text-danger"><?php echo e($errors->first('latitude')); ?>

                    </span>      
                  </div>
                </div>  
                <div class="form-group row">
                  <div class="col-4">
                    <label>Longitude
                    </label>
                    <input type="text" class="form-control" id="longitude" placeholder="Enter longitude(Optional)" name="lng" >
                    <span class="text-danger"><?php echo e($errors->first('longitude')); ?>

                    </span>      
                  </div>-->
                  <div class="col-4">
                    <label>Reck Management
                    </label>
                     <div class="form-group row">
                      <div class="col">
                        <label>Yes
                        <input type="radio" class="form-control" id="reck_management"  name="reck_management" value="yes" ></label>
                          <label>No
                        <input type="radio" class="form-control" id="reck_management"  name="reck_management" value="no" ></label>
                  </div>

                </div>
                    <span class="text-danger"><?php echo e($errors->first('reck_management')); ?></span>
                  </div>

                </div>

                <button type="submit" class="btn btn-primary mr-2">Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH /home/ct/public_html/finance/resources/views/warehouse/create.blade.php ENDPATH**/ ?>