<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{

	  protected $table = 'tbl_roles';
	  
        public function updateRoles($data)
{
        $roles = $this->find($data['id']);
        $roles->name = $data['name'];
        $roles->status = $data['status'];
        $roles->save();
        return 1;
}
}
