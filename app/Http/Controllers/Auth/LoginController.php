<?php



namespace App\Http\Controllers\Auth;

use App\User;



use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;



class LoginController extends Controller

{

    /*

    |--------------------------------------------------------------------------

    | Login Controller

    |--------------------------------------------------------------------------

    |

    | This controller handles authenticating users for the application and

    | redirecting them to your home screen. The controller uses a trait

    | to conveniently provide its functionality to your applications.

    |

    */



    use AuthenticatesUsers;



    /**

     * Where to redirect users after login.

     *

     * @var string

     */

    protected $redirectTo = '/home';



    // protected function credentials(Request $request)

    // {

    //   return ['email' => $request->email, 'password' => $request->password, 'status' => 'Active'];

    // }



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('guest')->except('logout');

    }



    protected function credentials(Request $request) {

      return  array_merge($request->only('email', 'password'), ['status' => 'Active']);

      

    }



    protected function sendFailedLoginResponse(Request $request)

    {

        $errors = ['email' => trans('auth.failed')];



        // Load user from database

        $user = User::where('email', $request->email)->first();



        // Check if user was successfully loaded, that the password matches

        // and active is not 1. If so, override the default error message.

        if ($user && \Hash::check($request->password, $user->password) && $user->status != 'Active') {

            $errors = ['email' => trans('Your Account has not been Activated yet, Please wait for Admin Approval.')];

        }



        if ($request->expectsJson()) {

            return response()->json($errors, 422);

        }

        return redirect()->back()

            ->withInput($request->only('email', 'remember'))

            ->withErrors($errors);

    }



}

