<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use App\User;

class RegistrationController extends Controller
{


    public function index(){
    	$user = DB::select('select * from users');
       // $user = User::all();
        return view('registration.index',compact('user'));
    }
 
    public function create(){
        return view('registration.create');
    }
 
    public function storeUser(Request $request){
 	
        $user = new User();
            $validatedData = $request->validate([
            'email' => 'bail|required|unique:users|max:255',
            'name'  => 'required|max:255',
            'password'  => 'required|min:8',

        ]);
        $user->name = request('name');
        $user->email = request('email');
        $user->password = request('password');
        $user->remember_token = request('_token');
        $user->status ='Active';
        $user->save();
        return redirect('/view');
 
    }

        public function edit($id)
    {
        $user = User::where(['id'=>$id])->first();
     //   return view('member.edit',compact('user'));
        return view('registration.edit', compact('user', 'id'));
    }

        public function destroy($id)
    {
        $ticket = User::find($id);
        $ticket->delete();

        return redirect('/view')->with('success', 'Ticket has been deleted!!');
    }


        public function update(Request $request, $id)
    {
        $user = new User();
        $data = $this->validate($request, [
            'name'=>'required',
            'email'=> 'required',
            'password'=> 'required',

        ]);
        $data['id'] = $id;
        $user->updateUser($data);

        return redirect('/view')->with('success', 'New support user has been updated!!');
    }


}
