<?php

namespace App\Http\Controllers;

use App\Import;
use App\ImportFile;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use Sunra\PhpSimple\HtmlDomParser;
use Orchestra\Parser\Xml\Facade as XmlParser;
use DB;
use Auth;
class ImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $list = ImportFile::all();
       return view('import.index',compact('list','roledropdown'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('import.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function xmlCreate()
    {
        return view('xml_import.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Import  $import
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $list = Import::where('url_id',$id)->first();
        return view('import.show',compact('list'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Import  $import
     * @return \Illuminate\Http\Response
     */
    public function edit(Import $import)
    {
        return view('import.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Import  $import
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Import $import)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Import  $import
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $list = Import::find($id)->first();
        $data=$list->delete();
        if($data){
            return redirect('/import_list')->with('message', 'Deleted Successfully!');
        }
        else{
            return redirect('/import_list')->with('message', 'SomeThing Went Wrong..!!!');
        }
    }

    /*
    * Import File
    */
    public function csvfileupload(Request $request)
    {
 
    DB::beginTransaction();
        try
        {          
        if ($request->hasFile('csvfile')) {
            $path = $request->file('csvfile')->getRealPath();
            $data = \Excel::load($path)->get();
             set_time_limit(0);
             ini_set("memory_limit", -1);
            if ($data->count() !=0) {

                foreach ($data as $key => $value) {
                    foreach ($value as $key1 => $val) {

                        $url = $val;
                        /* insert data */
                            \DB::table('tbl_import_file')->insert(
                                        array(
                                            'url'           => $url,
                                            'status'          => '1'                                                    
                                        )
                                    );     
                    }


                    
                }
            }
         }
        DB::commit();
                if($url){
                  return redirect('/import_list')->with('message', 'Added Successfully!');
                }
                else{
                    return redirect('/import_list')->with('message', 'SomeThing Went Wrong..!!!');
                }
        }
        catch(\Exception $e)
        {
            DB::rollBack();
            Log::emergency($e);
        }
        return redirect()->back();                        
      }
/* Store XML File */
    public function storeXml(Request $request)
    {
         
        if ($request->hasFile('file')) {
            $path = $request->file('file')->getRealPath();
           $fileName = time().'.'.$request->file->extension();  
            $request->file->move(public_path('uploads/xml_file'), $fileName);
            echo $file = public_path('uploads/xml_file/').$fileName;
            $xml=simplexml_load_file("https://rss.nytimes.com/services/xml/rss/nyt/World.xml") or die("Error: Cannot create object");

            //print_r($path);exit;
           // $xml = XmlParser::extract("/public/reports/abc.xml");
            //

            //$xml=simplexml_load_string();

            //$xml = simplexml_load_file($request->file('file')->getRealPath());
           // $xml = new \SimpleXMLElement($request->file('file')->getRealPath(), 0, true);

            //$xml = XmlParser::load($path);
            print_r($xml);exit;
            //$xml_object = simplexml_load_file($request->file('file')->getRealPath());

            //print_r($xml_object);exit;
        }
       // return redirect()->back();                             
        
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Import  $import
     * @return \Illuminate\Http\Response
     */
    public function bulkDelete(Request $request)
    {
        DB::beginTransaction();
        try
        {          
            $ids = $request->ids;
            DB::table("tbl_import")->whereIn('id',explode(",",$ids))->delete();
        DB::commit();    
            return response()->json(['success'=>"Deleted successfully."]);
        }
        catch(\Exception $e)
        {
            DB::rollBack();
            Log::emergency($e);
        }
        return redirect()->back();          
    }

}

