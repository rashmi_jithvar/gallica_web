<?php



namespace App\Http\Controllers;



use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Import;
use App\Patientdetail;
use Auth;
use DB;



class ApiController extends Controller

{



    public function signup(Request $request)

    {

        $user = new User();

        $user->name = $request->name;

        $user->email = $request->email;

        $user->user_type = $request->user_type;

        $user->status = $request->status;

        $user->password = Hash::make($request->password);

        $user->save();

        if (!empty($user->id)) {

            return ['status'=>'success','userData'=>$user];

        } else {

            return ['status'=>'failed'];

        }

    }



    public function addpatient(Request $request)

    {



        set_time_limit(0);

        $ids = $request->ids;

        // var_dump($ids);

        for ($i=0; $i < sizeof($ids); $i++) {

            $user = User::select('id','email')->where(['id'=>$ids[$i]])->first();

            $email = $user->email;



            $detail  = new Patientdetail();

            $detail->name = $request->name;

            $detail->site_hos = $request->site_hos;

            $detail->mrn_ramq = $request->patient_mrn;

            $detail->email = $request->email;

            $detail->email_mail = $email;

            $detail->detail = $request->patient_return;

            $detail->other_detail = $request->diagnosis;

            $detail->contact = $request->contact_no;

            $detail->save();

            Mail::send([], array('yourValue' => 'OTP'), function($message) use ($request,$email) {

                $body = file_get_contents('./email.html');

                $body = str_replace('$$email$$', $request->email, $body);

                $body = str_replace('$$patient$$', $request->name, $body);

                $body = str_replace('$$hospital$$', $request->site_hos, $body);

                $body = str_replace('$$contact$$', $request->contact, $body);

                $body = str_replace('$$patient_mrn$$', $request->mrn_ramq, $body);

                $body = str_replace('$$returning$$', $request->detail, $body);

                $body = str_replace('$$diagnosis$$', $request->other_detail, $body);



                $message->setBody($body, 'text/html');

                $message->to($email);

                $message->subject('New Patient Info');

            });



            if (Mail::failures()) {

                continue;

            }



        }

    }



    public function checkLogin(Request $request)

    {

        $whereconditoin = ['email'=>$request->email,'password'=>$request->password, 'status' => 'Active'];

        if (Auth::attempt($whereconditoin)) {

            $user = User::where(['id'=>Auth::user()->id])->first();

            return ['status'=>'success','userData'=>$user];

        }else{

            return ['status'=>'failed'];

        }

    }



    public function forgetpassword(Request $request)

    {

        set_time_limit(0);

        $whereconditoin = ['email'=>$request->email];

        $user = User::select('id','email')->where($whereconditoin)->first();

        if ( !empty($user) ) {



            $code = rand(1111,99999);

            $email = $user->email;

            $user->otp_mail = $code;

            $user->save();

            Mail::send([], array('yourValue' => 'OTP'), function($message) use ($code,$email) {

                $MailBody = 'OTP for change password :- '.$code;

                $message->setBody($MailBody, 'text/html');

                $message->to($email);

                $message->subject('OTP');

            });



            return ['status'=>'success','detail'=>'OTP send on your email id'];

        }else{

            return ['status'=>'failed','detail'=>'Email id is not found'];

        }

    }


    /**
    * Check Docket API
     */
    public function GetAWBNumber(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required','AWB_NO' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = User::where('id', '=', $request->user_id)
                //->where('status', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                $get_awb_list = DB::table('tbl_import')
                    ->whereIn('docket', [$request->AWB_NO])
                    ->get();
            
                if($get_awb_list){

                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            'data' => $get_awb_list,
                            'message' => 'Data Found..!!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                }  
                    else{
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            'data' => 'Null',
                            'message' => 'Data Not Found.!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
            }               
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Required Data Missing',
                    'errors' => $validator->errors(),
                ), RESPONSE_CODE_ERROR_MISSINGDATA
            );
        } else {
            $CheckUser = User::where('email', '=', $request->email)->first();

            if (!empty($CheckUser)) {

                if (password_verify($request->password, $CheckUser->password) || ($request->password == $CheckUser->otp)) {
                 
                    if ($CheckUser->status == ENUM_STATUS_ACTIVE) {
                        //company logic check
                        $CheckUser->token = uniqid();
                        $CheckUser->token_time = new \DateTime();
                        $CheckUser->save();
                       // $CheckUser->setAttribute('profile_image',$CheckUser->fetchImage());
                        if($CheckUser->status)
                        {
                            return response()->json(
                                array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    'data' => $CheckUser,
                                    'message' => 'User logged in successfully',
                                    'errors' => null,
                                ), RESPONSE_CODE_SUCCESS_OK
                            );
                        }
                    } else {

                        if ($CheckUser->status_enum == ENUM_STATUS_INACTIVE) {
                            return response()->json(
                                array(
                                    'status' => false,
                                    'code' => RESPONSE_CODE_ERROR_BAD,
                                    'data' => null,
                                    'message' => 'User status Inactive. Please contact admin.',
                                    'errors' => null,
                                ), RESPONSE_CODE_ERROR_BAD
                            );
                        }
                    }
                } else {
                    return response()->json(
                        array(
                            'status' => false,
                            'code' => RESPONSE_CODE_ERROR_UNAUTHORIZED,
                            'data' => null,
                            'message' => 'Incorrect password',
                            'errors' => null,
                        ), RESPONSE_CODE_ERROR_UNAUTHORIZED
                    );
                }
            } else {
                return response()->json(
                    array(
                        'status' => false,
                        'code' => RESPONSE_CODE_ERROR_BAD,
                        'data' => null,
                        'message' => 'Email not registered',
                        'errors' => null,
                    ), RESPONSE_CODE_ERROR_BAD
                );
            }
        }
    }    


}

