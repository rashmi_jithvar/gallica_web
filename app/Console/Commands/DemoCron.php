<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ImportFile;
use App\Import;
use Sunra\PhpSimple\HtmlDomParser;
use DB;
use Auth;

class DemoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $import = ImportFile::where('flag_id','0')->offset(0)->limit(10)->get();
        if(!empty($import)){
            foreach ($import as $key => $value) {
            $saveFlag = ImportFile::where('id',$value->id)->first();
                $url = $value->url;
                           $ch = curl_init();
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_URL, $url);
                                $result = curl_exec($ch);
                                curl_close($ch);

                                $html = HtmlDomParser::str_get_html($result);
                        
                                $arr = array();
                                foreach ($html->find('dd') as $v) {
                                    $arr[] = $v->innertext;
                                   
                                }
                                $arr2 = array(); 
                                foreach ($html->find('span[id="descriptionCollapseChild"]') as $s) {
                                    $arr2[] = $s->innertext;
                                }
                                $arr3 = array(); 
                                foreach ($html->find('span') as $s) {
                                    $arr3[] = $s->innertext;
                                }                                 
                                $links = array();                                                             
                                foreach($html->find('a') as $a) {
                                 $links[] = $a->href;
                                }
                                $images = array();
                                foreach($html->find('img') as $img) {
                                 $images[] = $img->src;
                                }  
                               // $arr ? $arr[15]:  '';
                                if(isset($arr[15])){
                                    $date_of_online_available = strtotime($arr[15]);
                                    $new_date_of_online_available = date('Y-m-d', $date_of_online_available);                                    
                                }
                                else{
                                    $new_date_of_online_available='0000-00-00';
                                }
                                if(isset($arr[2])){
                                    $publisher = $arr[2];
                                }
                                else{
                                    $publisher = '';
                                }
                                if(isset($arr2[2])){
                                    $description = $arr2[2];
                                }
                                else{
                                    $description = '';
                                }
                                /* provenanace */
                                if(isset($arr[14])){
                                    $provenance = $arr[14];
                                }
                                else{
                                    $provenance = '';
                                } 
                                /* title */
                                if(isset($arr[0])){
                                    $title = $arr[0];
                                }
                                else{
                                    $title = '';
                                }
                                /* Author */
                                if(isset($arr[1])){
                                    $author = $arr[1];
                                }
                                else{
                                    $author = '';
                                }   
                                /* publication_date */
                                if(isset($arr[3])){
                                    $publication_date = $arr[3];
                                }
                                else{
                                    $publication_date = '';
                                } 
                                /* type */
                                if(isset($arr[4])){
                                    $type = $arr[4];
                                }
                                else{
                                    $type = '';
                                } 
                                /* type */
                                if(isset($arr[5])){
                                    $language = $arr[5];
                                }
                                else{
                                    $language = '';
                                } 
                                /* type */
                                if(isset($arr[6])){
                                    $sub_language = $arr[6];
                                }
                                else{
                                    $sub_language = '';
                                } 
                                /* Format */
                                if(isset($arr2[0])){
                                    $format = $arr2[0];
                                }
                                else{
                                    $format = '';
                                } 
                                /* Format */
                                if(isset($arr2[1])){
                                    $format_1 = $arr2[1];
                                    
                                }
                                else{
                                    $format_1 = '';
                                } 
                                /* no of pages
                                */
                                if(isset($arr2[1])){
                                $page = (explode(":",$arr2[1]));
                                if(isset($page[1])){
                                    $page =  $page[1];    
                                }
                                else{
                                        $page =  '';
                                    }                                 
                                }
                                else{
                                    $page = '';
                                }                                 
                                /* rights */
                                if(isset($arr[10])){
                                    $rights = $arr[10];
                                }
                                else{
                                    $rights = '';
                                }   
                                /* source */
                                if(isset($arr[12])){
                                    $source = $arr[12];
                                }
                                else{
                                    $source = '';
                                } 
                                /* identifier */
                                if(isset($links[124])){
                                    $identifier = $links[124];
                                }
                                else{
                                    $identifier= '';
                                } 
                                /* relation */
                                if(isset($links[125])){
                                    $relation = $links[125];
                                }
                                else{
                                    $relation= '';
                                }                                 
                                $current_timestamp = strtotime("now");
                                $insert = DB::table('tbl_import')->insert(
                                        array(
                                            'title'           => $title,
                                            'author'          => $author,
                                            'publisher'       => $publisher,
                                            'publication_date' => $publication_date,
                                            'type'           => $type,
                                            'sub_type'      => 'text',
                                            'language'      => $language,
                                            'sub_language'  => $sub_language,
                                            'format'        => $format,
                                            'format_1'      => $format_1,
                                            'no_of_pages'      => $page,
                                            'description'   => $description,
                                            'rights'        => $rights,
                                            'identifier'    => $identifier,
                                            'source'        => $source,
                                            'relation'      => $relation,
                                            'provenance'    => $provenance ,
                                            'date_of_online_available'    => $new_date_of_online_available,
                                            'file_id'=> $value->id,
                                          'created_at' =>$current_timestamp                                                    
                                        )
                                    );
                    if($insert ==1){
                        $saveFlag->flag_id ='1';
                        $saveFlag->save();
                    }            
                        
            }
        }
        \Log::info("saved");
     
        /*
           Write your database logic we bellow:
           Item::create(['name'=>'hello new']);
        */
      
        $this->info('Demo:Cron Cummand Run successfully!');
    }
}
