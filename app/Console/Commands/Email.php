<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Process;

class Email extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $process = new Process();
        $process->name = 'status';
        $process->status = '1';
        $process->save();
        if (empty($process->errors)) {
            return '1';
        } else {
            return '0';
        }
    }
}
