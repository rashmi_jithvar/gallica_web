<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportFile extends Model
{
    protected $table = 'tbl_import_file';
    public $timestamps = false;
}
